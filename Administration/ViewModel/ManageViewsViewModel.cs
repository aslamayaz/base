﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Grid;

namespace Administration.ViewModel
{
  public class ManageViewsViewModel : BaseViewModel, IStandardView
  {
    private readonly ICustomizerRepository _customizerRepository;


    #region Implementation of IStandardView


    public bool CanAdd { get; set; } = false;

    public ModuleAccess ModuleAccess { get; set; }

    public void Export()
    {
      base.ExportToXlsx("ManageViews.xlsx");
    }

    public void AddRecord()
    {
    }

    public void EditRecord()
    {
    }

    public void DeleteRecord()
    {

    }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = base.SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      base.DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = base.UpdateViewLayout(layout);
      return updateViewLayout;
    }

    #endregion

    public ObservableCollection<BaTSControlCustomizer> ControlCustomizers
    {
      get { return GetValue<ObservableCollection<BaTSControlCustomizer>>(); }
      set { SetValue(value); }
    }


    public BaTSControlCustomizer SelectedControlCustomizer
    {
      get { return GetValue<BaTSControlCustomizer>(); }
      set { SetValue(value); }
    }

    public ManageViewsViewModel(ICustomizerRepository customizerRepository, ICurrentUser currentUser)
      : base(customizerRepository, currentUser)
    {
      _customizerRepository = customizerRepository;
      ControlCustomizers = new ObservableCollection<BaTSControlCustomizer>();
    }


    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
            x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();


        CheckEntityColumns(columnCustomizers, typeof(BaTSControlCustomizer));

        Columns = generateColumns(columnCustomizers);

        var collection = new ObservableCollection<BaTSControlCustomizer>();

        var controlCustomizers = _customizerRepository.CustomizerContext.BaTSControlCustomizers;

        foreach (var controlCustomizer in controlCustomizers)
        {
          collection.Add(controlCustomizer);
        }

        ControlCustomizers = collection;
      });
    }


    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        var newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
          ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
          : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);

        if (columnName.Equals("ControlName"))
        {
          newColumn.GroupIndex = 0;
        }

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }

    [Command]
    public void HandleControlCustomizer(RowEventArgs args)
    {
      var customizer = args.Row as BaTSControlCustomizer;
      _customizerRepository.UpdateControlCustomizer(customizer);
    }


    [Command]
    public void Delete(BaTSControlCustomizer controlCustomizer)
    {

    }

  }
}