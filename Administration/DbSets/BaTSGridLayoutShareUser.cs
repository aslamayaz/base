﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Administration.DbSets
{
    public partial class BaTSGridLayoutShareUser
    {
        public int Id { get; set; }
        public int LayoutId { get; set; }
        public int UserId { get; set; }
        public bool? OpenFirst { get; set; }
    }
}
