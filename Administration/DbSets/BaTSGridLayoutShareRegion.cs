﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Administration.DbSets
{
    public partial class BaTSGridLayoutShareRegion
    {
        public int Id { get; set; }
        public int LayoutId { get; set; }
        public int GlobalRegionId { get; set; }
        public bool? OpenFirst { get; set; }
    }
}
