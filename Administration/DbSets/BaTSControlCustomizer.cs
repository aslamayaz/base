﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Administration.DbSets
{
    public partial class BaTSControlCustomizer
    {
        public int Id { get; set; }
        public string ControlName { get; set; }
        public string ColumnName { get; set; }
        public string ColumnTextOrigin { get; set; }
        public string ColumnTextCustomize { get; set; }
        public decimal? ColumnWidth { get; set; }
        public int? ColumnPosition { get; set; }
        public string Status { get; set; }
        public string DbtableName { get; set; }
        public string DbcolumnName { get; set; }
        public string ColumnType { get; set; }
        public string ShortHelpText { get; set; }
        public string LongHelpText { get; set; }
        public bool? WordWrap { get; set; }
        public decimal? ExcelWidth { get; set; }
        public int? CellBackground { get; set; }
        public int? HeaderBackGround { get; set; }
        public string Datasource { get; set; }
        public string Comment { get; set; }
        public string BandName { get; set; }
        public int? BandColIndex { get; set; }
        public int? BandRowIndex { get; set; }
        public DateTime? LastFound { get; set; }
    }
}
