﻿using System;
using System.Collections.Generic;
using System.Linq;
using Commons.Enums;
using DevExpress.Mvvm;

namespace Base.Models
{
  public class ItemDescriptionBase
  {
    protected ItemDescriptionBase(string title, IList<ItemDescription> items, NavigationViewModelBase navigationViewModel = null)
    {
      Title = title;
      Items = new List<ItemDescription>(items ?? Enumerable.Empty<ItemDescription>());
      ShowExpandButton = true;
      NavigationViewModel = navigationViewModel;
    }

    public string Title { get; protected set; }
    public string GroupTitle { get; protected set; }
    public bool HasAccess { get; set; }

    public ModuleAccess AccessRight { get; set; }

    public bool CanAdd { get; set; }

    public Uri Icon { get; protected set; }
    public virtual bool ShowExpandButton { get; set; }

    public NavigationViewModelBase NavigationViewModel { get; protected set; }

    public virtual IList<ItemDescription> Items { get; set; }
  }
}