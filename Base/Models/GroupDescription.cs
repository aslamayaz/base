﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Mvvm.POCO;

namespace Base.Models
{
  public class GroupDescription : ItemDescriptionBase
  {
    public static GroupDescription Create(string title, IList<ItemDescription> items = null, bool showChildrenExpandButton = false, Uri icon = null)
    {
      return ViewModelSource.Create(() => new GroupDescription(title, items, showChildrenExpandButton, icon));
    }
    protected GroupDescription(string title, IList<ItemDescription> items, bool showChildrenExpandButton, Uri icon)
      : base(title, items)
    {
      Icon = icon;
      if (Items.Any())
      {
        SelectedItem = Items.First();
      }

      foreach (var item in Items)
      {
        item.ShowExpandButton = showChildrenExpandButton;
      }
    }
    public virtual ItemDescription SelectedItem { get; set; }
  }
}