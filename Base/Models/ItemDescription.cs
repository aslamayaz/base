﻿using System;
using System.Collections.Generic;
using Commons.Enums;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace Base.Models
{
  public class ItemDescription : ItemDescriptionBase
  {
    public static ItemDescription Create(string title, string groupTitle, bool hasAccess, ModuleAccess accessRight, IList<ItemDescription> items = null, NavigationViewModelBase navigationViewModel = null, Uri icon = null)
    {
      return ViewModelSource.Create(() => new ItemDescription(title, groupTitle, hasAccess, accessRight, icon, items, navigationViewModel));
    }
    protected ItemDescription(string title, string groupTitle, bool hasAccess, ModuleAccess accessRight, Uri icon, IList<ItemDescription> items, NavigationViewModelBase navigationViewModel)
      : base(title, items, navigationViewModel)
    {
      GroupTitle = groupTitle;
      Icon = icon;
      HasAccess = hasAccess;
      AccessRight = accessRight;

      if (title != "Users")
      {
        CanAdd = true;
      }
    }
  }
}