﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Administration.ViewModel;
using Administration.Views;
using Base.ViewModels;
using Commons;
using Commons.Repositories;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Core;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Security.ActiveDirectory;
using Security.ViewModel;
using Security.Views;
using Unity;
using Unity.Lifetime;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using ModulesViewModel = Security.ViewModel.ModulesViewModel;

namespace Base
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    #region Overrides of Application

    protected override void OnExit(ExitEventArgs e)
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();
      securityRepository.SetUserStatus(currentUser.Id, false, currentUser.SessionThread);
    }

    protected override async void OnStartup(StartupEventArgs e)
    {
      Commons.Utils.Container.UnityContainer = new UnityContainer();

      Commons.Utils.Container.UnityContainer.RegisterType<ISecurityRepository, SecurityRepository>();
      Commons.Utils.Container.UnityContainer.RegisterType<ICustomizerRepository, CustomizerRepository>();
      Commons.Utils.Container.UnityContainer.RegisterType<ICurrentUser, CurrentUser>(
        new ContainerControlledLifetimeManager());


      Commons.Utils.Container.UnityContainer.RegisterType(typeof(MainViewModel),
        ViewModelSource.GetPOCOType(typeof(MainViewModel)));

      Commons.Utils.Container.UnityContainer.RegisterType(typeof(UserViewModel),
        ViewModelSource.GetPOCOType(typeof(UserViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(UserDetailViewModel),
        ViewModelSource.GetPOCOType(typeof(UserDetailViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(UserGroupsViewModel),
        ViewModelSource.GetPOCOType(typeof(UserGroupsViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(UserMonitorViewModel),
        ViewModelSource.GetPOCOType(typeof(UserMonitorViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(ModulesViewModel),
        ViewModelSource.GetPOCOType(typeof(ModulesViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(ManageViewsViewModel),
        ViewModelSource.GetPOCOType(typeof(ManageViewsViewModel)));
      Commons.Utils.Container.UnityContainer.RegisterType(typeof(SettingsViewModel),
        ViewModelSource.GetPOCOType(typeof(SettingsViewModel)));

      registerViews();

      //ApplicationThemeHelper.ApplicationThemeName = Theme.Office2019BlackName;


      var databaseResponse = checkDatabaseConnection();

      if (!databaseResponse.Item1)
      {
        DXMessageBox.Show(
          $"{databaseResponse.Item2}", "Access Denied", MessageBoxButton.OK, MessageBoxImage.Error);

        Current.Shutdown();
        return;
      }
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();

      var setups = securityRepository.SecurityContext.BaTSSetups.ToArray();

      createDefaultSetupEntries(setups);


      var currentWindowLogin = Environment.UserDomainName + @"\" + Environment.UserName;

      try
      {
        var isCurrentUserInitiated = initiateCurrentUser(currentWindowLogin);

        if (!isCurrentUserInitiated)
        {
          DXMessageBox.Show(
            $"CurrentUser with the Login {currentWindowLogin} can not be found in the database. Application will be closed!",
            "Loading of the user failed!", MessageBoxButton.OK, MessageBoxImage.Error);

          Current.Shutdown();
          return;
        }
      }
      catch (Exception exception)
      {
        DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

        Current.Shutdown();
        return;
      }


      //Todo check against these entries

      var maintenanceMode = setups.First(x => x.Area.Equals("Application") && x.Criteria.Equals("IsInMaintencanceMode"))
        .Value
        .Equals("1");

      var maintenanceModeMessage =
        setups.First(x => x.Area.Equals("Application") && x.Criteria.Equals("MaintenanceModeMessage")).Value;


      if (maintenanceMode)
      {
        MessageBox.Show(maintenanceModeMessage, "Maintenance Mode", MessageBoxButton.OK, MessageBoxImage.Error);
        Current.Shutdown();
        return;
      }

      var minVersion = setups.First(x => x.Area.Equals("Application") && x.Criteria.Equals("MinVersion")).Value;

      var assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version?.ToString();

      if (!currentUser.IsDeveloper && !IsActualVersionValid(assemblyVersion, minVersion))
      {
        MessageBox.Show(
          $"Version {assemblyVersion} of the application is out of date. Please get in contact with the admin.",
          "Version out of date", MessageBoxButton.OK, MessageBoxImage.Error);
        //Application.Exit();
        //return;
      }


      //var confidentialMessage = setups
      //  .First(x => x.Area.Equals("Application") && x.Criteria.Equals("ConfidentialMessage")).Value;

      //var result = MessageBox.Show(confidentialMessage, string.Empty, MessageBoxButton.OKCancel,
      //  MessageBoxImage.Question);

      //SplashScreenHelper.ShowSplashScreen();

      ActiveDirectory.ImportActiveDirectory();


      Task.Run(() => setUserStatus(true));
      Task.Run(() => getUserMessages());
      Task.Run(() => kickCurrentUserOut());

      //if (result == MessageBoxResult.OK)
      //{
      var mainWindow = Commons.Utils.Container.UnityContainer.Resolve<MainWindow>(); // Creating Main window
      try
      {
        mainWindow.Show();
      }
      catch (Exception exception)
      {
        DXMessageBox.Show(exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
      }
      //}
      //else
      //{
      //  Current.Shutdown();
      //}
    }


    private bool initiateCurrentUser(string currentWindowLogin)
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();

      var user = securityRepository.SecurityContext.BaTDUsers
        .FirstOrDefault(x => x.WindowsLogin.Equals(currentWindowLogin));

      if (user == null)
      {
        return false;
      }

      currentUser.Id = user.Id;

      currentUser.WindowsLogin = user.WindowsLogin;

      currentUser.Username = $"{user.Firstname} {user.LastName}";

      currentUser.IsDeveloper = securityRepository.SecurityContext.BaTSSetups.Any(x =>
        x.Area.Equals("SpecialUserRoles") &&
        x.Criteria.Equals("IsDeveloper") &&
        x.Value.Equals(user.Id.ToString()));

      currentUser.IsAdmin = securityRepository.SecurityContext.BaTSSetups.Any(x =>
        x.Area.Equals("SpecialUserRoles") &&
        x.Criteria.Equals("IsAdmin") &&
        x.Value.Equals(user.Id.ToString()));

      var param = new SqlParameter("@UserID", user.Id);
      var userPolicies = securityRepository.SecurityContext.Set<UserPolicy>()
        .FromSqlRaw("Security.BA_usp_Select_User_Policies @UserID", param);

      currentUser.UserPolicies = userPolicies.ToArray();

      if (!currentUser.UserPolicies.Any())
      {
        DXMessageBox.Show(
          $"CurrentUser has no access to the tool. Please contact admin",
          "Application will be closed!", MessageBoxButton.OK, MessageBoxImage.Error);

        Current.Shutdown();
        return false;
      }

      return true;
    }

    private async void setUserStatus(bool status)
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();
      var guid = Guid.NewGuid();
      currentUser.SessionThread = guid.ToString();
      while (true)
      {
        securityRepository.SetUserStatus(currentUser.Id, status, currentUser.SessionThread);

      }
    }

    private async void getUserMessages()
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();
      var lastMessage = string.Empty;

      while (true)
      {
        var messageSent = securityRepository.SecurityContext.BaTDConnectedUsers
          .FirstOrDefault(x => x.UserId == currentUser.Id && x.SessionThread == currentUser.SessionThread)?.SendMessage;

        if (!string.IsNullOrEmpty(messageSent) && lastMessage != messageSent)
        {
          lastMessage = messageSent;
          MessageBox.Show(messageSent, "Received Message from Admin", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        await Task.Delay(5000);
      }
    }

    private async void kickCurrentUserOut()
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();
      var currentUser = Commons.Utils.Container.UnityContainer.Resolve<ICurrentUser>();

      while (true)
      {
        var closeApplication = securityRepository.SecurityContext.BaTDConnectedUsers
          .FirstOrDefault(x => x.UserId == currentUser.Id && x.SessionThread == currentUser.SessionThread)?.CloseApplication;

        if (closeApplication.HasValue && closeApplication.Value)
        {
          Application.Current.Dispatcher.Invoke(() =>
          {
            securityRepository.SetUserStatus(currentUser.Id, false, currentUser.SessionThread);

            Current.Shutdown();
          }, DispatcherPriority.Background);
        }

        await Task.Delay(5000);
      }
    }


    private (bool, string) checkDatabaseConnection()
    {
      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();

      try
      {
        securityRepository.Context.Database.OpenConnection();
        var isConnected = securityRepository.Context.Database.CanConnect();

        return (isConnected, "Could not connect to the database");
      }
      catch (Exception exception)
      {
        return (false, exception.Message);
      }
    }

    #endregion

    private void registerViews()
    {
      (ViewLocator.Default as LocatorBase)?.RegisterType("Users", typeof(User));
      (ViewLocator.Default as LocatorBase)?.RegisterType("UserGroups", typeof(UserGroups));
      (ViewLocator.Default as LocatorBase)?.RegisterType("UserDetail", typeof(UserDetail));
      (ViewLocator.Default as LocatorBase)?.RegisterType("UserMonitor", typeof(UserMonitor));
      (ViewLocator.Default as LocatorBase)?.RegisterType("Modules", typeof(Modules));
      (ViewLocator.Default as LocatorBase)?.RegisterType("ManageViews", typeof(ManageViews));
      (ViewLocator.Default as LocatorBase)?.RegisterType("Settings", typeof(Settings));
    }

    public bool IsActualVersionValid(string actualVersion, string applicationMinVersion)
    {
      var result = false;

      var arrActualVersion = actualVersion.Split('.');
      var arrSetupEntry = applicationMinVersion.Split('.');
      var actualVersionNumber = 0.0;
      var actualSetupVersionNumber = 0.0;

      actualVersionNumber = Convert.ToDouble(arrActualVersion[0]) * 1000000000 +
                            Convert.ToDouble(arrActualVersion[1]) * 1000000 +
                            Convert.ToDouble(arrActualVersion[2]) * 1000 +
                            Convert.ToDouble(arrActualVersion[3]);

      actualSetupVersionNumber = Convert.ToDouble(arrSetupEntry[0]) * 1000000000 +
                                 Convert.ToDouble(arrSetupEntry[1]) * 1000000 +
                                 Convert.ToDouble(arrSetupEntry[2]) * 1000 +
                                 Convert.ToDouble(arrSetupEntry[3]);

      if (actualVersionNumber >= actualSetupVersionNumber)
      {
        result = true;
      }

      return result;
    }

    private List<BaTSSetup> getDefaultSetupEntries()
    {
      var setups = new List<BaTSSetup>
      {
        new BaTSSetup()
        {
          Area = "Application", Criteria = "MinVersion", Value = "1.0.0.0",
          Comment = "Min Version that the application must have to start."
        },
        new BaTSSetup()
        {
          Area = "Application", Criteria = "IsInMaintencanceMode", Value = "0",
          Comment = "Maintenance Mode;0 if no; 1 if yes."
        },
        new BaTSSetup()
        {
          Area = "Application", Criteria = "MaintenanceModeMessage", Value = "Application is in Maintenance mode.",
          Comment = "Message that users will get if the application is in maintenance mode."
        },
        new BaTSSetup()
        {
          Area = "Application", Criteria = "ConfidentialMessage",
          Value = "Application contains confidential data. If you proceed....",
          Comment = "Message that user will see if the application will be started."
        },
        //Special UserRoles
        new BaTSSetup()
        {
          Area = "SpecialUserRolesDefinitions", Criteria = "IsAdmin",
          Value = "User of this role has special admin rights.",
          Comment = "Min Version that the application must have to start."
        },
        new BaTSSetup()
        {
          Area = "SpecialUserRolesDefinitions", Criteria = "IsDeveloper",
          Value = "User of this role has special developer rights.",
          Comment = "Min Version that the application must have to start."
        }
      };

      return setups;
    }

    private void createDefaultSetupEntries(IEnumerable<BaTSSetup> setups)
    {
      var defaultEntries = getDefaultSetupEntries();

      var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();

      foreach (var defaultEntry in defaultEntries)
      {
        var hasEntry = setups.Any(ele => ele.Area != null &&
                                         ele.Criteria != null && ele.Value != null &&
                                         ele.Area.ToString().ToLower() == defaultEntry.Area.ToString().ToLower() &&
                                         ele.Criteria.ToString().ToLower() ==
                                         defaultEntry.Criteria.ToString().ToLower());

        if (!hasEntry)
        {
          securityRepository.SaveSetup(-1, defaultEntry);
        }
      }
    }
  }
}