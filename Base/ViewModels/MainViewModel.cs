﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using Base.Models;
using Commons;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Layouts;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Accordion;
using DevExpress.Xpf.Core;

namespace Base.ViewModels
{
  public class MainViewModel : ViewModelBase
  {

    #region Properties

    private readonly ICustomizerRepository _customizerRepository;

    public ICurrentUser CurrentUser { get; set; }

    protected IDocumentManagerService DocumentManagerService
    {
      get { return this.GetRequiredService<IDocumentManagerService>(); }
    }

    protected IMessageBoxService MessageBoxService
    {
      get { return this.GetRequiredService<IMessageBoxService>(); }
    }

    protected ILayoutService LayoutService
    {
      get { return this.GetRequiredService<ILayoutService>(); }
    }

    public ObservableCollection<GroupDescription> Groups
    {
      get { return GetValue<ObservableCollection<GroupDescription>>(); }
      set { SetValue(value); }
    }

    public GroupDescription SelectedGroup
    {
      get { return GetValue<GroupDescription>(); }
      set
      {
        //if (value.SelectedItem == null)
        //{
        //  value.SelectedItem = value.Items.First();
        //}

        SetValue(value);
      }
    }

    public bool CanLayout
    {
      get { return GetValue<bool>(); }
      set { SetValue(value); }
    }

    public string LayoutName
    {
      get { return GetValue<string>(); }
      set { SetValue(value); }
    }

    public bool LayoutOpenFirst
    {
      get { return GetValue<bool>(); }
      set { SetValue(value); }
    }

    public ObservableCollection<BaTSGridLayout> Layouts
    {
      get { return GetValue<ObservableCollection<BaTSGridLayout>>(); }
      set { SetValue(value); }
    }

    public BaTSGridLayout SelectedLayout
    {
      get { return GetValue<BaTSGridLayout>(); }
      set
      {
        SetValue(value);
        if (value != null)
        {
          var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
          viewModel.LayoutService.ReloadLayout(value);
        }
      }
    }

    public virtual bool OnStartup { get; set; }

    #endregion

    #region TabSelectionChangedEventHandler

    [Command]
    public void TabSelectionChanged(TabControlSelectionChangedEventArgs changedEventArgs)
    {
      CanLayout = false;

      if (changedEventArgs.NewSelectedItem != null)
      {
        var dataContext = (changedEventArgs.NewSelectedItem as DXTabItem)?.DataContext;

        if (dataContext != null)
        {
          var document = DocumentManagerService.FindDocument(dataContext);

          if (document.Content is IStandardView viewModel)
          {
            CanLayout = true;
            SelectedGroup = Groups.FirstOrDefault(x => x.Title.Equals(viewModel?.ParameterViewModel.GroupTitle));
            if (SelectedGroup != null)
            {
              SelectedGroup.SelectedItem = SelectedGroup.Items.First(x => x.Title.Equals(document.Title));

              //setLayouts(viewModel.ParameterViewModel.Title);
            }
          }
        }
      }

      if (SelectedGroup != null && changedEventArgs.NewSelectedIndex == -1)
      {
        SelectedGroup.SelectedItem = null;
      }
    }

    #endregion

    #region ctor

    public MainViewModel(ICustomizerRepository customizerRepository, ICurrentUser currentUser)
    {
      OnStartup = true;

      _customizerRepository = customizerRepository;
      CurrentUser = currentUser;

      Layouts = new ObservableCollection<BaTSGridLayout>();
    }

    #endregion

    #region Navigation

    public void OnLoaded()
    {
      Task.Run(initNavigation).Wait();

      if (OnStartup && Groups.Count > 0)
      {
        //OnSelectedItemChanged(new AccordionSelectedItemChangedEventArgs(null, null,
        //  Groups.First().Items.First(x => x.AccessRight != ModuleAccess.NoAccess)));
      };
    }

    private void initNavigation()
    {
      var navigation = CurrentUser.UserPolicies.OrderBy(x => x.MenuGroupOrderPosition)
        .GroupBy(
          p => p.MenuItemGroup,
          p => new
          {
            p.MenuItemName,
            Icon = p.IconImageName,
            Access = (ModuleAccess)Enum.Parse(typeof(ModuleAccess), p.AccessRight)
          },
          (key, g) => new { Title = key, SubItems = g.ToList() });

      var groups = new ObservableCollection<GroupDescription>();

      foreach (var navItem in navigation)
      {
        var hasNoAccessToModuleGroup = navItem.SubItems.All(x => x.Access == ModuleAccess.NoAccess);

        if (hasNoAccessToModuleGroup)
        {
          continue;
        }

        var groupDescription = GroupDescription.Create(navItem.Title);

        foreach (var subItem in navItem.SubItems)
        {
          var hasAccessToSubItem = subItem.Access != ModuleAccess.NoAccess;
          groupDescription.Items.Add(ItemDescription.Create(subItem.MenuItemName, navItem.Title, hasAccessToSubItem,
            subItem.Access));
        }

        groups.Add(groupDescription);
      }

      Groups = groups;

      if (Groups.Count > 0)
      {
        SelectedGroup = Groups.First();
      }
    }

    public void OnSelectedItemChanged(AccordionSelectedItemChangedEventArgs e)
    {
      if (e.OldItem != null)
      {
        OnStartup = false;
      }

      var newItem = e.NewItem as ItemDescription;
      if (newItem == null)
      {
        return;
      }

      var nameOfView = Regex.Replace(newItem.Title, @"\s+", "");

      var document = DocumentManagerService.Documents.FirstOrDefault(x => x.Title.Equals(newItem.Title));

      if (document == null)
      {
        document = DocumentManagerService.CreateDocument(nameOfView,
          new ParameterViewModel() { Title = newItem.Title, GroupTitle = newItem.GroupTitle }, this);

        document.Title = newItem.Title;
        document.DestroyOnClose = true;

        if (document.Content is IStandardView view)
        {
          view.LoadData();
          view.ModuleAccess = newItem.AccessRight;
          CanLayout = true;
        }
      }

      document.Show();

      //setLayouts(newItem.Title);
    }

    #endregion

    #region Layouts

    [Command]
    public void GetLayout()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;


      Layouts = _customizerRepository.CustomizerContext.BaTSGridLayouts.Where(x =>
          x.Form.Equals(viewModel.ParameterViewModel.Title) && x.UserId.Equals(CurrentUser.Id))
        .ToObservableCollection();
    }

    #endregion

    #region Framework Commands


    [Command]
    public void Export()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
      viewModel?.Export();
    }

    [Command]
    public void Add()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
      viewModel?.AddRecord();
    }

    [Command]
    public void Delete()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
      viewModel?.DeleteRecord();
    }

    [Command]
    public void Refresh()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
      viewModel?.RefreshRecords();
    }


    [Command]
    public void Edit()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;
      viewModel?.EditRecord();
    }

    [Command]
    public void AddLayout()
    {

      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;

      var registerCommand = new UICommand()
      {
        Caption = "OK",
        IsDefault = true,
        Command = new DelegateCommand(() => { }, () => !string.IsNullOrEmpty(LayoutName))
      };

      var cancelCommand = new UICommand()
      {
        Caption = "Cancel",
        IsCancel = true,
      };

      var service = this.GetService<IDialogService>("DialogService");

      var result = service.ShowDialog(
        dialogCommands: new[] { registerCommand, cancelCommand },
        title: "Layout Name",
        viewModel: this
      );

      if (result.IsCancel)
      {
        return;
      }

      var layout = viewModel?.SaveLayout(LayoutName, true);

      if (layout != null)
      {
        Layouts.Add(layout);
      }
    }


    [Command]
    public void UpdateLayout()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;

      if (SelectedLayout != null)
      {
        viewModel?.UpdateLayout(SelectedLayout);
      }
      else
      {
        MessageBoxService.ShowMessage($"Select a layout to update", "Layout", MessageButton.OK, MessageIcon.Information);
      }
    }

    [Command]
    public void DeleteLayout()
    {
      var viewModel = DocumentManagerService.ActiveDocument.Content as IStandardView;

      if (SelectedLayout != null)
      {
        viewModel?.DeleteLayout(SelectedLayout);
      }
      else
      {
        MessageBoxService.ShowMessage($"Select a layout to delete", "Layout", MessageButton.OK, MessageIcon.Information);
      }
    }

    [Command]
    public void CloseWindow()
    {
      Application.Current.Shutdown();
    }

    [Command]
    public void RestartTool()
    {
      System.Windows.Forms.Application.Restart();
      Application.Current.Shutdown();
    }

    #endregion

  }
}