﻿namespace WpfCore.ViewModels
{
  public interface IStandardView
  {
    void Export();

    void AddRecord();

    void EditRecord();
  }
}