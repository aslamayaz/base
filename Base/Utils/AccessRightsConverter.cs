﻿using System;
using System.Globalization;
using System.Windows.Data;
using Commons.Enums;
using Commons.Interfaces;
using DevExpress.Xpf.Core;

namespace Base.Utils
{
  public class AccessRightsConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {

      if (value is DXTabItem { DataContext: IStandardView view })
      {
        switch (view.ModuleAccess)
        {
          case ModuleAccess.Read:
            return false;
          case ModuleAccess.ReadWrite:
            return true;
        }
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}