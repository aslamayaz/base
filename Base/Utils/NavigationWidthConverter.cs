﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Base.Utils
{
  public class NavigationWidthConverter : MarkupExtension, IValueConverter
  {
    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      return this;
    }
    public double StartupWidth { get; set; }
    bool isExpanded = true;

    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      isExpanded = (bool)value;
      return isExpanded ? new GridLength(StartupWidth) : GridLength.Auto;
    }
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (isExpanded)
      {
        StartupWidth = ((GridLength)value).Value;
      }

      return Binding.DoNothing;
    }

  }
}