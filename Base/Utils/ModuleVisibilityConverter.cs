﻿using System;
using System.Globalization;
using System.Windows.Data;
using Commons.Interfaces;
using DevExpress.Xpf.Core;

namespace Base.Utils
{
  public class ModuleVisibilityConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is DXTabItem { DataContext: IStandardView { CanAdd: true } })
      {
        return true;
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}