﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Markup;
using Unity;

namespace WpfCore.Utils
{
  public class ContainerExtension : MarkupExtension
  {
    private readonly Type _type;
    private object _dataContext;

    public ContainerExtension(Type type)
    {
      _type = type;
    }

    public override object ProvideValue(IServiceProvider serviceProvider)
    {
      if (serviceProvider == null)
      {
        throw new ArgumentNullException("serviceProvider");
      }

      if (_type == null)
      {
        throw new NullReferenceException("Type");
      }

      if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
      {
        return null;
      }

      try
      {
        _dataContext = App.Container.Resolve(_type);
      }
      catch (Exception ex)
      {
        return null;
      }

      return _dataContext;
    }
  }
}