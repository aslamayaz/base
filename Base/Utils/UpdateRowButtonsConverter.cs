﻿using System;
using System.Globalization;
using System.Windows.Data;
using Commons.Enums;

namespace Base.Utils
{
  public class UpdateRowButtonsConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is ModuleAccess moduleAccess)
      {
        if (moduleAccess == ModuleAccess.ReadWrite)
        {
          return "OnCellEditorOpen";
        }
        else
        {
          return "Never";
        }
      }

      return "Naver";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}