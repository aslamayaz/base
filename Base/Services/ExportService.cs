﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;

namespace WpfCore.Services
{
  public class ExportService : ServiceBase, IExportService
  {
    private IServiceContainer _serviceContainer;

    #region Implementation of IExportService

    public void Export(string name)
    {
      var view = ((GridControl)AssociatedObject).View;
      ((TableView)view).ExportToXlsx(name);
    }


    #endregion
  }
}