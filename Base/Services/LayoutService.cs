﻿using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;
using System.IO;

namespace WpfCore.Services
{
  public class LayoutService : ServiceBase, ILayoutService
  {
    #region Implementation of ILayoutService

    public void SaveLayout(MemoryStream stream)
    {
      var gridControl = ((GridControl)AssociatedObject);
      gridControl.SaveLayoutToStream(stream);

    }

    public void ReoadLayout(MemoryStream stream)
    {
      var gridControl = ((GridControl)AssociatedObject);

      stream.Position = 0;
      gridControl.RestoreLayoutFromStream(stream);
    }

    #endregion
  }
}