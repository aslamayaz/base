﻿using System.IO;

namespace WpfCore.Services
{
  public interface ILayoutService
  {
    void SaveLayout(MemoryStream stream);
    void ReoadLayout(MemoryStream stream);
  }
}