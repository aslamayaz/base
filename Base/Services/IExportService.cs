﻿namespace WpfCore.Services
{
  public interface IExportService
  {
    void Export(string name);
  }
}