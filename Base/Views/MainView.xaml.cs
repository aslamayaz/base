﻿using System.Collections;
using System.Windows.Controls;
using Base.Models;
using DevExpress.Xpf.Accordion;

namespace Base.Views
{
  /// <summary>
  /// Interaction logic for View1.xaml
  /// </summary>
  public partial class MainView : UserControl
  {
    public MainView()
    {
      InitializeComponent();
    }
  }

  public class NavigationChildrenSelector : IChildrenSelector
  {
    IEnumerable IChildrenSelector.SelectChildren(object item)
    {
      if (item is GroupDescription)
      {
        return ((GroupDescription)item).Items;
      }
      else if (item is ItemDescription)
      {
        return ((ItemDescription)item).Items;
      }
      return null;
    }
  }
}
