﻿using DevExpress.Xpf.Core;

namespace Base
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : ThemedWindow
  {
    public MainWindow()
    {
      InitializeComponent();
    }
  }
}
