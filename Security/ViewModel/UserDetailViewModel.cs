﻿using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Security.ViewModel
{
  public class UserDetailViewModel : BindableBase, ISupportParameter
  {
    private readonly ISecurityRepository _securityRepository;

    public UserDetailViewModel(ISecurityRepository securityRepository)
    {
      _securityRepository = securityRepository;
      SpecialUserRolesDefinitions = new ObservableCollection<BaTSSetup>();
      AssignedSpecialRoles = new ObservableCollection<BaTSSetup>();
    }

    public virtual UserDetailParameter UserDetailParameter { get; set; }

    private object _Parameter;

    object ISupportParameter.Parameter
    {
      get { return _Parameter; }
      set
      {
        _Parameter = value;
        UserDetailParameter = (UserDetailParameter)_Parameter;
      }
    }

    public ObservableCollection<BaTSSetup> SpecialUserRolesDefinitions
    {
      get { return GetValue<ObservableCollection<BaTSSetup>>(); }
      set { SetValue(value); }
    }

    public ObservableCollection<BaTSSetup> AssignedSpecialRoles
    {
      get { return GetValue<ObservableCollection<BaTSSetup>>(); }
      set { SetValue(value); }
    }

    public async void LoadSpecialUserRoles()
    {
      await Task.Run(() =>
      {
        var newSpecialRoles = new ObservableCollection<BaTSSetup>();
        var newAssignRoles = new ObservableCollection<BaTSSetup>();

        var setups = _securityRepository.SecurityContext.BaTSSetups.ToArray();

        var specialUserRolesDefinitions =
          setups.Where(x => x.Area != null && x.Area.Equals("SpecialUserRolesDefinitions")).ToArray();

        var assignedSpecialRoles = setups.Where(x => x.Area != null &&
                                                     x.Area.Equals("SpecialUserRoles") &&
                                                     x.Value != null &&
                                                     x.Value.Equals(
                                                       UserDetailParameter.User.Id.ToString())).ToArray();


        foreach (var specialUserRolesDefinition in specialUserRolesDefinitions)
        {
          if (assignedSpecialRoles.Any(x => x.Criteria.Equals(specialUserRolesDefinition.Criteria)))
          {
            continue;
          }

          newSpecialRoles.Add(specialUserRolesDefinition);
        }

        foreach (var assignedSpecialRole in assignedSpecialRoles)
        {
          newAssignRoles.Add(assignedSpecialRole);
        }

        SpecialUserRolesDefinitions = newSpecialRoles;
        AssignedSpecialRoles = newAssignRoles;
      });
    }


    [Command]
    public void Back()
    {
      var service = this.GetRequiredService<IDocumentManagerService>();
      service.ActiveDocument.Close();
    }

    [Command]
    public void Add()
    {
      _securityRepository.UpdateUser(UserDetailParameter);
    }

    [Command]
    public void CellValueChanged(object obj)
    {
      var policy = (UserPolicy)(obj as CellValue)?.Row;

      policy.AccessRight = (obj as CellValue)?.Value.ToString();

      _securityRepository.SaveUserModuleAccess(policy);
    }

    [Command]
    public void AssignSpecialRole(BaTSSetup setup)
    {
      var newSetupEntry = new BaTSSetup()
      {
        Area = "SpecialUserRoles",
        Criteria = setup.Criteria,
        Value = UserDetailParameter.User.Id.ToString(),
        LastUpdateOn = DateTime.Now,
        LastUpdateBy = UserDetailParameter.User.WindowsLogin,
        Comment = $"{UserDetailParameter.User.Firstname} {UserDetailParameter.User.LastName}:{setup.Value}"
      };

      _securityRepository.SaveSetup(-1, newSetupEntry);
      LoadSpecialUserRoles();
    }

    [Command]
    public void DeleteAssignRole(BaTSSetup setup)
    {
      _securityRepository.DeleteSetup(setup);
      LoadSpecialUserRoles();
    }
  }
}