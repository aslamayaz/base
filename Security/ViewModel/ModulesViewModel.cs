﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Grid;

namespace Security.ViewModel
{
  public class ModulesViewModel : BaseViewModel, IStandardView, ISupportParameter
  {
    private readonly ISecurityRepository _securityRepository;
    private readonly ICustomizerRepository _customizerRepository;


    public ObservableCollection<BaTSModule> Modules
    {
      get { return GetValue<ObservableCollection<BaTSModule>>(); }
      set { SetValue(value); }
    }


    public BaTSModule SelectedModule
    {
      get { return GetValue<BaTSModule>(); }
      set { SetValue(value); }
    }


    public ModulesViewModel(ISecurityRepository securityRepository, ICustomizerRepository customizerRepository, ICurrentUser currentUser)
      : base(customizerRepository, currentUser)
    {
      _securityRepository = securityRepository;
      _customizerRepository = customizerRepository;
      Modules = new ObservableCollection<BaTSModule>();
    }

    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
            x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();

        CheckEntityColumns(columnCustomizers, typeof(BaTSModule));

        Columns = generateColumns(columnCustomizers);

        var collection = new ObservableCollection<BaTSModule>();

        var modules = _securityRepository.SecurityContext.BaTSModules;

        foreach (var module in modules)
        {
          collection.Add(module);
        }

        Modules = collection;
      });
    }


    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        var newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
          ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
          : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }


    #region Implementation of IStandardView

    public bool CanAdd { get; set; } = false;

    public ModuleAccess ModuleAccess { get; set; }

    public void Export()
    {
      base.ExportToXlsx("Modules.xlsx");
    }

    public void AddRecord()
    {

    }

    public void EditRecord()
    {

    }

    public void DeleteRecord()
    {

    }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = base.SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      base.DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = base.UpdateViewLayout(layout);
      return updateViewLayout;
    }


    #endregion


    [Command]
    public void HandleModule(RowEventArgs args)
    {

    }


    [Command]
    public void Delete(BaTSModule user)
    {

    }

  }
}