﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Grid;

namespace Security.ViewModel
{
  public class UserGroupsViewModel : BaseViewModel, IStandardView
  {

    private readonly ISecurityRepository _securityRepository;
    private readonly ICustomizerRepository _customizerRepository;

    public ObservableCollection<BaTDUserGroup> UserGroups
    {
      get { return GetValue<ObservableCollection<BaTDUserGroup>>(); }
      set { SetValue(value); }
    }

    public BaTDUserGroup SelectedUserGroup
    {
      get { return GetValue<BaTDUserGroup>(); }
      set { SetValue(value); }
    }


    public UserGroupsViewModel(ISecurityRepository securityRepository, ICustomizerRepository customizerRepository, ICurrentUser currentUser)
    : base(customizerRepository, currentUser)
    {
      _securityRepository = securityRepository;
      _customizerRepository = customizerRepository;
      UserGroups = new ObservableCollection<BaTDUserGroup>();
    }


    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
            x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();

        CheckEntityColumns(columnCustomizers, typeof(BaTDUserGroup));

        Columns = generateColumns(columnCustomizers);

        var collection = new ObservableCollection<BaTDUserGroup>();

        var userGroups = _securityRepository.SecurityContext.BaTDUserGroups;
        foreach (var userGroup in userGroups)
        {
          collection.Add(userGroup);
        }

        UserGroups = collection;
      });
    }


    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        var newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
           ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
           : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }

    [Command]
    public void HandleUserGroupCommand(RowEventArgs args)
    {
      if (args.Row is not BaTDUserGroup userGroup)
      {
        return;
      }

      if (userGroup.Id != 0)
      {
        _securityRepository.SaveUserGroup(userGroup.Id, userGroup.GroupName, userGroup.IsActive);
      }
      else if (userGroup.Id == 0)
      {
        _securityRepository.SaveUserGroup(-1, userGroup.GroupName, userGroup.IsActive);
      }
    }


    [Command]
    public void Delete(BaTDUserGroup userGroup)
    {
      _securityRepository.DeleteUserGroup(userGroup);
      UserGroups.Remove(userGroup);
    }

    public bool CanAdd { get; set; } = true;
    public ModuleAccess ModuleAccess { get; set; }

    [Command]
    public void Export()
    {
      base.ExportToXlsx("UserGroups.xlsx");
    }

    public void AddRecord()
    {
      GridService.AddNewRow();
    }


    public void EditRecord()
    {
      if (SelectedUserGroup == null)
      {
        MessageBoxService.Show("Please select a user group to edit", "Edit User Group", MessageBoxButton.OK,
          MessageBoxImage.Exclamation);
        return;
      }

      GridService.EditRow();
    }

    public void DeleteRecord()
    {
      if (SelectedUserGroup == null)
      {
        MessageBoxService.Show("Please select a user group to delete", "Delete User Group", MessageBoxButton.OK,
          MessageBoxImage.Exclamation);
        return;
      }

      _securityRepository.DeleteUserGroup(SelectedUserGroup);
      UserGroups.Remove(SelectedUserGroup);
    }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = base.SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      base.DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = base.UpdateViewLayout(layout);
      return updateViewLayout;
    }
  }
}