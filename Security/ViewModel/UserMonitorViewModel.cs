﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DbSets.Security;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Xpf;

namespace Security.ViewModel
{
  public class UserMonitorViewModel : BaseViewModel, IStandardView
  {
    private readonly ISecurityRepository _securityRepository;
    private readonly ICustomizerRepository _customizerRepository;
    private readonly ICurrentUser _currentUser;

    public ObservableCollection<UserMonitorEntity> UserMonitors
    {
      get { return GetValue<ObservableCollection<UserMonitorEntity>>(); }
      set { SetValue(value); }
    }

    public BaTDUserGroup SelectedUserMonitor
    {
      get { return GetValue<BaTDUserGroup>(); }
      set { SetValue(value); }
    }


    public UserMonitorViewModel(ISecurityRepository securityRepository, ICustomizerRepository customizerRepository, ICurrentUser currentUser)
    : base(customizerRepository, currentUser)
    {
      _securityRepository = securityRepository;
      _customizerRepository = customizerRepository;
      _currentUser = currentUser;
      UserMonitors = new ObservableCollection<UserMonitorEntity>();
    }


    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
            x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();

        CheckEntityColumns(columnCustomizers, typeof(UserMonitorEntity));

        Columns = generateColumns(columnCustomizers);

        var collection = new ObservableCollection<UserMonitorEntity>();

        var userMonitors = _securityRepository.GetUserMonitor();

        foreach (var userMonitor in userMonitors)
        {
          collection.Add(userMonitor);
        }

        UserMonitors = collection;
      });
    }

    private void logOffUser()
    {
      _securityRepository.LogOffUser(_currentUser.Id);
    }

    private void sendMessageToUser()
    {
      _securityRepository.SendMessageToUSer(_currentUser.Id, "");
    }




    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        var newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
           ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
           : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }

    [Command]
    public void HandleUserGroupCommand(RowValidationArgs args)
    {
      var userGroup = args.Item as BaTDUserGroup;
      if (userGroup == null)
      {
        return;
      }

      if (!args.IsNewItem)
      {
        _securityRepository.SaveUserGroup(userGroup.Id, userGroup.GroupName, userGroup.IsActive);
      }
      else if (args.IsNewItem)
      {
        _securityRepository.SaveUserGroup(-1, userGroup.GroupName, userGroup.IsActive);
      }
    }


    [Command]
    public void DeleteRow(BaTDUserGroup userGroup)
    {

    }

    public bool CanAdd { get; set; } = true;
    public ModuleAccess ModuleAccess { get; set; }

    [Command]
    public void Export()
    {
      base.ExportToXlsx("UserGroups.xlsx");
    }

    public void AddRecord()
    {
      GridService.AddNewRow();
    }


    public void EditRecord()
    {

    }

    public void DeleteRecord()
    {

    }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = base.SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      base.DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = base.UpdateViewLayout(layout);
      return updateViewLayout;
    }
  }
}