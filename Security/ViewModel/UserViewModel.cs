﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.POCO;
using DevExpress.Xpf.Grid;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Security.ViewModel
{
  public class UserViewModel : BaseViewModel, IStandardView
  {
    private readonly ISecurityRepository _securityRepository;
    private readonly ICustomizerRepository _customizerRepository;

    public ObservableCollection<BaTDUser> Users
    {
      get { return GetValue<ObservableCollection<BaTDUser>>(); }
      set { SetValue(value); }
    }

    public UserDetailParameter UserDetailParameter { get; set; }

    public BaTDUser SelectedUser
    {
      get { return GetValue<BaTDUser>(); }
      set { SetValue(value); }
    }


    public UserViewModel(ISecurityRepository securityRepository, ICurrentUser currentUser,
      ICustomizerRepository customizerRepository) : base(customizerRepository, currentUser)
    {
      _securityRepository = securityRepository;
      _customizerRepository = customizerRepository;
      Users = new ObservableCollection<BaTDUser>();
      Columns = new ObservableCollection<Column>();
    }

    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var users = _securityRepository.SecurityContext.BaTDUsers;

        var newUsers = new ObservableCollection<BaTDUser>();

        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
           x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();

        CheckEntityColumns(columnCustomizers, typeof(BaTDUser));

        Columns = generateColumns(columnCustomizers);

        foreach (var user in users)
        {
          newUsers.Add(user);
        }

        Users = newUsers;
      });
    }


    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        Column newColumn;
        if (columnName.Equals("GroupId"))
        {
          var source = _securityRepository.SecurityContext.BaTDUserGroups.ToList();
          newColumn = new LookupColumn(ColumnType.Lookup, columnName, source, nameof(BaTDUserGroup.GroupName),
            baTsControlCustomizer.IsReadOnly ?? true);
        }
        else
        {
          newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
            ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
            : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);
        }

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }

    [Command]
    public void HandleUser(RowEventArgs args)
    {
      var user = args.Row as BaTDUser;

      var parameter = new UserDetailParameter
      {
        User = user
      };

      _securityRepository.UpdateUser(parameter);
    }


    [Command]
    public void Delete(BaTDUser user)
    {
      _securityRepository.DeleteUser(user.Id);
      Users.Remove(user);
    }


    public bool CanAdd { get; set; }
    public ModuleAccess ModuleAccess { get; set; }

    [Command]
    public void Export()
    {
      ExportToXlsx("Users.xlsx");
    }


    public void AddRecord()
    { }

    public void EditRecord()
    {
      if (SelectedUser == null)
      {
        return;
      }

      Edit(SelectedUser);
    }

    public void DeleteRecord()
    { }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = UpdateViewLayout(layout);
      return updateViewLayout;
    }


    [Command]
    public void Edit(BaTDUser user)
    {
      if (user == null)
      {
        return;
      }

      UserDetailParameter = new UserDetailParameter
      {
        User = user,
        UserPolicies = new ObservableCollection<UserPolicy>(),
        GroupTitle = ParameterViewModel.GroupTitle
      };


      var param = new SqlParameter("@UserID", user.Id);

      var userPolicies = _securityRepository.SecurityContext.Set<UserPolicy>()
        .FromSqlRaw("Security.BA_usp_Select_User_Policies @UserID", param);

      if (userPolicies != null)
      {
        foreach (var userPolicy in userPolicies)
        {
          UserDetailParameter.UserPolicies.Add(userPolicy);
        }
      }

      var service = this.GetRequiredService<IDocumentManagerService>();
      var document = service.FindDocument(user, this);

      if (document == null)
      {
        document = service.CreateDocument("UserDetail", UserDetailParameter,
          this);
        document.Title = $"{user.Firstname} {user.LastName}";
      }

      var viewModel = document.Content as UserDetailViewModel;
      viewModel?.LoadSpecialUserRoles();
      document.Show();
    }
  }
}