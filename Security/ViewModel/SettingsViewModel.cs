﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Commons;
using Commons.ColumnCustomizer;
using Commons.Enums;
using Commons.Interfaces;
using Commons.Repositories;
using Commons.ViewModel;
using DbSets.Customizer;
using DbSets.Security;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Xpf.Grid;

namespace Security.ViewModel
{
  public class SettingsViewModel : BaseViewModel, IStandardView
  {
    private readonly ISecurityRepository _securityRepository;
    private readonly ICustomizerRepository _customizerRepository;

    #region Implementation of IStandardView

    public bool CanAdd { get; set; } = true;

    public ModuleAccess ModuleAccess { get; set; }

    public void Export()
    {
      base.ExportToXlsx("Settings.xlsx");
    }

    public void AddRecord()
    {
      GridService.AddNewRow();
    }


    public void EditRecord()
    {
      if (SelectedSetup == null)
      {
        MessageBoxService.Show("Please select a setting to edit", "Edit setup", MessageBoxButton.OK,
          MessageBoxImage.Exclamation);
        return;
      }

      GridService.EditRow();
    }



    public void DeleteRecord()
    {
      if (SelectedSetup == null)
      {
        MessageBoxService.Show("Please select a user group to delete", "Delete User Group", MessageBoxButton.OK,
          MessageBoxImage.Exclamation);
        return;
      }

      _securityRepository.DeleteSetup(SelectedSetup);
      Setups.Remove(SelectedSetup);
    }

    public void RefreshRecords()
    {
      LoadData();
    }

    public BaTSGridLayout SaveLayout(string name, bool openFirst)
    {
      var layout = base.SaveViewLayout(name, openFirst, false);
      return layout;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      base.DeleteViewLayout(layout);
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updateViewLayout = base.UpdateViewLayout(layout);
      return updateViewLayout;
    }

    #endregion


    public ObservableCollection<BaTSSetup> Setups
    {
      get { return GetValue<ObservableCollection<BaTSSetup>>(); }
      set { SetValue(value); }
    }


    public BaTSSetup SelectedSetup
    {
      get { return GetValue<BaTSSetup>(); }
      set { SetValue(value); }
    }



    public SettingsViewModel(ISecurityRepository securityRepository, ICustomizerRepository customizerRepository, ICurrentUser currentUser)
      : base(customizerRepository, currentUser)
    {
      _securityRepository = securityRepository;
      _customizerRepository = customizerRepository;
      Setups = new ObservableCollection<BaTSSetup>();
    }

    public async void LoadData()
    {
      await Task.Run(() =>
      {
        var columnCustomizers =
          ((CustomizerContext)_customizerRepository.Context).BaTSControlCustomizers.Where(x =>
            x.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")).ToArray();

        CheckEntityColumns(columnCustomizers, typeof(BaTSSetup));

        Columns = generateColumns(columnCustomizers);

        var collection = new ObservableCollection<BaTSSetup>();

        var setups = _securityRepository.SecurityContext.BaTSSetups;

        foreach (var setup in setups)
        {
          collection.Add(setup);
        }

        Setups = collection;
      });
    }

    private ObservableCollection<Column> generateColumns(IEnumerable<BaTSControlCustomizer> columnCustomizers)
    {
      var newColumns = new ObservableCollection<Column>();

      foreach (var baTsControlCustomizer in columnCustomizers)
      {
        var columnName = baTsControlCustomizer.ColumnName.Trim();

        var newColumn = baTsControlCustomizer.ColumnType.Equals("bool")
          ? new Column(ColumnType.Bool, columnName, baTsControlCustomizer.IsReadOnly ?? true)
          : new Column(ColumnType.Default, columnName, baTsControlCustomizer.IsReadOnly ?? true);

        if (!string.IsNullOrWhiteSpace(baTsControlCustomizer.ColumnTextCustomize))
        {
          newColumn.Header = baTsControlCustomizer.ColumnTextCustomize;
        }

        if (baTsControlCustomizer.WordWrap.HasValue)
        {
          newColumn.WordWrap = baTsControlCustomizer.WordWrap.Value;
        }


        newColumns.Add(newColumn);
      }

      return newColumns;
    }


    [Command]
    public void HandleSettings(RowEventArgs args)
    {
      if (args.Row is not BaTSSetup setup)
      {
        return;
      }

      if (setup.Id != 0)
      {
        _securityRepository.SaveSetup(setup.Id, setup);
      }
      else if (setup.Id == 0)
      {
        _securityRepository.SaveSetup(-1, setup);
      }
    }


    [Command]
    public void Delete(BaTSSetup setup)
    {
      _securityRepository.DeleteSetup(setup);
      Setups.Remove(setup);
    }
  }
}