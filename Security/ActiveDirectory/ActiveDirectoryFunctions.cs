﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;

namespace Security.ActiveDirectory
{
  public class ActiveDirectoryFunctions
  {
    public static List<ActiveDirectoryEntry> GetMemberOfGroup(string group)
    {
      var adMembers = new List<ActiveDirectoryEntry>();

      try
      {
        using (DirectorySearcher searcher = new DirectorySearcher(new DirectoryEntry(string.Empty)))
        {
          searcher.Filter = $"(&(objectClass=group)(cn={group}))";
          foreach (SearchResult result in searcher.FindAll())
          {
            foreach (var member in result.Properties["member"])
            {
              using (DirectoryEntry memberResult = new DirectoryEntry($"LDAP://{member}"))
              {
                string username = null;
                try
                {
                  username = memberResult.Properties["sAMAccountName"].Value as string;
                }
                catch (Exception)
                {
                  username = memberResult.Path.Substring(0, memberResult.Path.IndexOf(',')).Replace("LDAP://CN=", string.Empty);
                }

                string displayname = null;
                try
                {
                  displayname = memberResult.Properties["displayName"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }
                string mail = null;
                try
                {
                  mail = memberResult.Properties["mail"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }

                string department = null;
                try
                {
                  department = memberResult.Properties["department"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }

                string firstName = null;
                try
                {
                  firstName = memberResult.Properties["givenName"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }

                string lastName = null;
                try
                {
                  lastName = memberResult.Properties["sn"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }


                string site = null;
                try
                {
                  site = memberResult.Properties["BASFCompanyID"].Value as string;
                }
                catch (Exception)
                {
                  // ignored
                }

                if (username != null)
                {
                  adMembers.Add(new ActiveDirectoryEntry(username, displayname, mail, null, null, @group, department, firstName, lastName, site));
                }
              }
            }
          }
        }
      }
      catch (Exception)
      {
        // ignored
      }

      return adMembers;
    }
  }
}
