﻿namespace Security.ActiveDirectory
{
  public class ActiveDirectoryEntry
  {
    public ActiveDirectoryEntry()
    {

    }

    public ActiveDirectoryEntry(string memberName, string displayName, string mail, int? region, string windowsLogin, string activeDirectoryUserGroup,
      string department, string firstName, string lastName, string site)
    {
      MemberName = memberName;
      DisplayName = displayName;
      Mail = mail;
      Region = region;
      WindowsLogin = windowsLogin;
      ActiveDirectoryUserGroup = activeDirectoryUserGroup;
      Department = department;
      FirstName = firstName;
      LastName = lastName;
      Site = site;

    }

    public string Site { get; set; }

    public string LastName { get; set; }

    public string FirstName { get; set; }

    public string Department { get; set; }

    public string ActiveDirectoryUserGroup { get; set; }

    public string WindowsLogin { get; set; }

    public int? Region { get; set; }

    public string Mail { get; set; }

    public string DisplayName { get; set; }

    public string MemberName { get; set; }
  }
}