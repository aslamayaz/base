﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Commons.Repositories;
using DbSets.Security;
using Unity;

namespace Security.ActiveDirectory
{
  public class ActiveDirectory
  {

    public static void ImportActiveDirectory()
    {
      try
      {

        var activeDirectoryUsers = new List<ActiveDirectoryEntry>();

        foreach (var dirEntry in ActiveDirectoryFunctions.GetMemberOfGroup("UG_APPL_CCB_BatMaps_User"))
        {
          activeDirectoryUsers.Add(new ActiveDirectoryEntry()
          {
            DisplayName = dirEntry.DisplayName,
            MemberName = dirEntry.MemberName,
            Mail = dirEntry.Mail ?? "",
            Region = 1,
            WindowsLogin = @"BASFAD\" + dirEntry.MemberName.ToUpper(),
            ActiveDirectoryUserGroup = dirEntry.ActiveDirectoryUserGroup,
            Department = dirEntry.Department,
            Site = dirEntry.Site
          });
        }


        var securityRepository = Commons.Utils.Container.UnityContainer.Resolve<ISecurityRepository>();

        var users = securityRepository.SecurityContext.BaTDUsers.ToList();


        var commonUsers = (from user in users
                           where user.WindowsLogin != null
                           join userActiveDirectory in activeDirectoryUsers on user.WindowsLogin.ToUpper() equals userActiveDirectory.WindowsLogin
                           select user).ToList();



        foreach (var commonUser in commonUsers)
        {
          var activeDirectoryUser =
            activeDirectoryUsers.First(x => x.WindowsLogin.Equals(commonUser.WindowsLogin.ToUpper()));

          if (commonUser.Email != activeDirectoryUser.Mail)
          {
            commonUser.Email = activeDirectoryUser.Mail;
            securityRepository.UpdateUser(commonUser);
          }
        }

        var listOfWindowsLogin = users.Where(x => x.WindowsLogin != null)
          .Select(x => x.WindowsLogin.ToUpper()).ToList();

        var newUsers = activeDirectoryUsers.Where(x => !listOfWindowsLogin.Contains(x.WindowsLogin));

        foreach (var user in newUsers)
        {
          var newUser = new BaTDUser
          {
            Id = -1,
            WindowsLogin = user.WindowsLogin,
            Firstname = "NEW USER",
            LastName = user.DisplayName,
            Email = user.Mail,
            LastUpdateOn = DateTime.Now
          };
          switch (user.Region)
          {
            case 1:
              {
                newUser.StandardCurrencyId = 1;
                break;
              }

            case 2:
              {
                newUser.StandardCurrencyId = 2;
                break;
              }

            case 3:
              {
                newUser.StandardCurrencyId = 3;
                break;
              }

            default:
              {
                newUser.StandardCurrencyId = 1;
                break;
              }
          }

          securityRepository.AddUser(newUser);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show("Check_ActiveDirectory_ForNew_Users-" + ex.Message);
      }
    }
  }
}