﻿using System.Collections.Generic;
using Commons.ViewModel;
using DbSets.Repository;
using DbSets.Security;

namespace Commons.Repositories
{
  public interface ISecurityRepository : IRepository
  {
    public SecurityContext SecurityContext { get; }

    public BaTDUser UpdateUser(UserDetailParameter parameter);

    public BaTDUser UpdateUser(BaTDUser user);

    public BaTDUser AddUser(BaTDUser user);

    public void SaveUserModuleAccess(UserPolicy selectedUserPolicy);

    public void DeleteUser(int UserId);

    public BaTDUserGroup SaveUserGroup(int id, string groupName, bool? isActive);

    public void DeleteUserGroup(BaTDUserGroup userGroup);

    BaTSSetup SaveSetup(int id, BaTSSetup setup);

    void DeleteSetup(BaTSSetup setup);

    void Init_ListOfDefaultSetupEntries(List<BaTSSetup> setups);

    IEnumerable<UserMonitorEntity> GetUserMonitor();

    void SetUserStatus(int currentUserId, bool status, string userThread);

    void LogOffUser(int currentUserId);

    void SendMessageToUSer(int currentUserId, string message);
  }
}
