﻿using DbSets.Customizer;
using DbSets.Repository;
using Microsoft.EntityFrameworkCore;

namespace Commons.Repositories
{
  public class CustomizerRepository : Repository, ICustomizerRepository
  {
    #region Overrides of Repository

    public override DbContext Context { get; }
    public CustomizerContext CustomizerContext { get; }

    public BaTSGridLayout SaveLayout(BaTSGridLayout layout)
    {
      var newLayout = CustomizerContext.BaTSGridLayouts.Add(layout);

      CustomizerContext.SaveChanges();

      return newLayout?.Entity;
    }

    public BaTSGridLayout UpdateLayout(BaTSGridLayout layout)
    {
      var updatedLayout = CustomizerContext.BaTSGridLayouts.Update(layout);

      CustomizerContext.SaveChanges();

      return updatedLayout?.Entity;
    }

    public void DeleteLayout(BaTSGridLayout layout)
    {
      CustomizerContext.BaTSGridLayouts.Remove(layout);

      CustomizerContext.SaveChanges();
    }

    public BaTSControlCustomizer SaveControlCustomizer(BaTSControlCustomizer controlCustomizer)
    {
      var updatedEntry = CustomizerContext.BaTSControlCustomizers.Add(controlCustomizer).Entity;
      CustomizerContext.SaveChanges();

      return updatedEntry;
    }

    public BaTSControlCustomizer UpdateControlCustomizer(BaTSControlCustomizer controlCustomizer)
    {
      var updatedEntry = CustomizerContext.BaTSControlCustomizers.Update(controlCustomizer).Entity;
      CustomizerContext.SaveChanges();

      return updatedEntry;
    }

    #endregion


    public CustomizerRepository(CustomizerContext customizerContext)
    {
      Context = customizerContext;
      CustomizerContext = customizerContext;
    }
  }
}