﻿using System.Collections.Generic;
using System.Linq;
using Commons.ViewModel;
using DbSets.Repository;
using DbSets.Security;
using Microsoft.EntityFrameworkCore;

namespace Commons.Repositories
{
  public class SecurityRepository : Repository, ISecurityRepository
  {
    private readonly ICurrentUser _currentUser;
    public override DbContext Context { get; }

    public SecurityContext SecurityContext { get; }

    public SecurityRepository(SecurityContext securityContext, ICurrentUser currentUser)
    {
      Context = securityContext;
      SecurityContext = securityContext;
      _currentUser = currentUser;
    }

    public BaTDUser UpdateUser(UserDetailParameter parameter)
    {
      var user = parameter.User;

      var updatedUser = (Context as SecurityContext)?.Set<BaTDUser>().FromSqlInterpolated(
          $"Security.BA_usp_Save_User {user.Id}, {user.LastName}, {user.Firstname}, {user.WindowsLogin}, {user.GroupId}, {user.GlobalRegionId}, {user.StandardCurrencyId}, {user.AllowToChangeCurrency}, {user.Culture}, {user.CultureUi}, {user.Email}, {user.IsActive}, {false}, {string.Empty}")
        .AsEnumerable().FirstOrDefault();

      return updatedUser;
    }

    public BaTDUser UpdateUser(BaTDUser user)
    {
      var updatedUser = SecurityContext.BaTDUsers.Update(user).Entity;
      SecurityContext.SaveChanges();

      return updatedUser;
    }

    public BaTDUser AddUser(BaTDUser user)
    {
      var updatedUser = SecurityContext.BaTDUsers.Add(user).Entity;
      SecurityContext.SaveChanges();

      return updatedUser;
    }

    public void SaveUserModuleAccess(UserPolicy selectedUserPolicy)
    {
      var userModuleAccess = (Context as SecurityContext)?.Database.ExecuteSqlInterpolated(
        $"EXECUTE [Security].[BA_usp_Save_User_Module_Access] {selectedUserPolicy.UserID}, {selectedUserPolicy.ModuleID}, {selectedUserPolicy.AccessRight}, {true}, {string.Empty}");
    }


    public void DeleteUser(int userId)
    {
      var deletedUser = (Context as SecurityContext)?.Set<BaTDUser>().FromSqlInterpolated(
          $"Security.BA_usp_Save_User {userId}, {false}, {string.Empty}")
        .AsEnumerable().FirstOrDefault();
    }

    public BaTDUserGroup SaveUserGroup(int id, string groupName, bool? isActive)
    {
      var userGroup = (Context as SecurityContext)?.Set<BaTDUserGroup>().FromSqlInterpolated(
          $"EXECUTE [Security].[BA_usp_Save_User_Group] {id}, {groupName}, {isActive}, {true}, {string.Empty}")
        .AsEnumerable().FirstOrDefault();

      return userGroup;
    }

    public void DeleteUserGroup(BaTDUserGroup userGroup)
    {
      var result = (Context as SecurityContext)?.BaTDUserGroups.Remove(userGroup);
      (Context as SecurityContext)?.SaveChanges();
    }


    public BaTSSetup SaveSetup(int id, BaTSSetup setup)
    {
      var newSetup = (Context as SecurityContext)?.Set<BaTSSetup>().FromSqlInterpolated(
          $"EXECUTE [Security].[BA_usp_Save_SetupEntry] {id}, {setup.Area}, {setup.Criteria}, {setup.Value}, {setup.Comment}, {true}, {string.Empty}")
        .AsEnumerable().FirstOrDefault();

      return newSetup;
    }

    public void DeleteSetup(BaTSSetup setup)
    {
      var result = (Context as SecurityContext)?.BaTSSetups.Remove(setup);
      (Context as SecurityContext)?.SaveChanges();
    }


    public void Init_ListOfDefaultSetupEntries(List<BaTSSetup> setups)
    {
      foreach (var baTsSetup in setups)
      {
        SaveSetup(-1, baTsSetup);
      }
    }

    public IEnumerable<UserMonitorEntity> GetUserMonitor()
    {
      var userMonitors = SecurityContext.Set<UserMonitorEntity>()
        .FromSqlInterpolated($"[Security].[BA_usp_Select_UserMonitor] {null}, {null}").AsEnumerable();

      return userMonitors;
    }

    public void SetUserStatus(int currentUserId, bool status, string userThread)
    {
      var result = SecurityContext.Database.ExecuteSqlInterpolated($"[Security].[BA_usp_Set_User_Status] {status}, {currentUserId}, {userThread}");
    }

    public void LogOffUser(int currentUserId)
    {
      var result = SecurityContext.Database.ExecuteSqlInterpolated($"[Security].[BA_usp_LoggOff_Users] {currentUserId}, {1}, {string.Empty}");
    }

    public void SendMessageToUSer(int currentUserId, string message)
    {
      var result = SecurityContext.Database.ExecuteSqlInterpolated($"[Security].[BA_usp_LoggOff_Users] {currentUserId}, {message}, {1}, {string.Empty}");
    }
  }
}