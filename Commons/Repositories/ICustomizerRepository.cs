﻿using DbSets.Customizer;
using DbSets.Repository;

namespace Commons.Repositories
{
  public interface ICustomizerRepository : IRepository
  {
    public CustomizerContext CustomizerContext { get; }
    BaTSGridLayout SaveLayout(BaTSGridLayout layout);

    BaTSGridLayout UpdateLayout(BaTSGridLayout layout);

    void DeleteLayout(BaTSGridLayout layout);

    BaTSControlCustomizer SaveControlCustomizer(BaTSControlCustomizer controlCustomizer);
    BaTSControlCustomizer UpdateControlCustomizer(BaTSControlCustomizer controlCustomizer);
  }
}