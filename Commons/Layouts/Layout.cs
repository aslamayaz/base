﻿using DevExpress.Mvvm;
using System.IO;

namespace Commons.Layouts
{
  public class Layout : BindableBase
  {
    public int Id { get; set; }

    public string Name
    {
      get
      {
        return GetValue<string>();
      }
      set
      {
        SetValue(value);
      }
    }

    public string Source
    {
      get
      {
        return GetValue<string>();
      }
      set
      {
        SetValue(value);
      }
    }

    public MemoryStream Stream
    {
      get
      {
        return GetValue<MemoryStream>();
      }
      set
      {
        SetValue(value);
      }
    }
  }
}