﻿using DbSets.Customizer;
using System.IO;

namespace Commons.Layouts
{
  public interface ILayoutService
  {
    MemoryStream SaveLayout();
    void ReloadLayout(BaTSGridLayout layout);
  }
}