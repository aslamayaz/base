﻿using DbSets.Customizer;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;
using System.IO;

namespace Commons.Layouts
{
  public class LayoutService : ServiceBase, ILayoutService
  {
    #region Implementation of ILayoutService

    public MemoryStream SaveLayout()
    {
      var stream = new MemoryStream();
      var gridControl = ((GridControl)AssociatedObject);
      gridControl.SaveLayoutToStream(stream);

      return stream;
    }

    public void ReloadLayout(BaTSGridLayout layout)
    {
      var gridControl = ((GridControl)AssociatedObject);

      var stream = new MemoryStream(layout.Layoutstream);
      stream.Position = 0;
      gridControl.RestoreLayoutFromStream(stream);
    }

    #endregion
  }
}