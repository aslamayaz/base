﻿using Commons.Enums;
using Commons.Layouts;
using Commons.ViewModel;
using DbSets.Customizer;

namespace Commons.Interfaces
{
  public interface IStandardView
  {
    public abstract ParameterViewModel ParameterViewModel { get; set; }

    public void LoadData();

    public ILayoutService LayoutService { get; }

    public bool CanAdd { get; set; }

    public ModuleAccess ModuleAccess { get; set; }

    void Export();

    void AddRecord();

    void EditRecord();

    void DeleteRecord();

    void RefreshRecords();

    BaTSGridLayout SaveLayout(string name, bool openFirst);

    void DeleteLayout(BaTSGridLayout layout);

    BaTSGridLayout UpdateLayout(BaTSGridLayout layout);
  }
}