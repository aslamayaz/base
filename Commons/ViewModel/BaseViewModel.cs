﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Commons.ColumnCustomizer;
using Commons.Layouts;
using Commons.Repositories;
using Commons.Services;
using Commons.Utils;
using DbSets.Customizer;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;

namespace Commons.ViewModel
{
  public class BaseViewModel : BindableBase, ISupportParameter
  {
    private readonly ICustomizerRepository _customizerRepository;
    private readonly ICurrentUser _currentUser;

    protected IExportService ExportService
    {
      get { return this.GetRequiredService<IExportService>(); }
    }

    protected IGridService GridService
    {
      get { return this.GetRequiredService<IGridService>(); }
    }


    public ILayoutService LayoutService
    {
      get { return this.GetRequiredService<ILayoutService>(); }
    }

    protected IMessageBoxService MessageBoxService
    {
      get { return this.GetRequiredService<IMessageBoxService>(); }
    }

    public ObservableCollection<Column> Columns
    {
      get { return GetValue<ObservableCollection<Column>>(); }
      set { SetValue(value); }
    }

    public BaseViewModel(ICustomizerRepository customizerRepository, ICurrentUser currentUser)
    {
      _customizerRepository = customizerRepository;
      _currentUser = currentUser;
      Columns = new ObservableCollection<Column>();
    }

    public virtual ParameterViewModel ParameterViewModel { get; set; }

    object _Parameter;

    object ISupportParameter.Parameter
    {
      get { return _Parameter; }
      set
      {
        _Parameter = value;
        ParameterViewModel = (ParameterViewModel)_Parameter;
      }
    }


    public void ExportToXlsx(string name)
    {
      var service = this.GetService<ISaveFileDialogService>("SaveFileDialogService");
      service.Filter = "Excel Files|*.xlsx;";
      service.DefaultFileName = name;
      service.DefaultExt = "xlsx";
      if (service.ShowDialog())
      {
        try
        {
          ExportService.Export(service.File.GetFullName());
          MessageBoxService.ShowMessage($"Successfully exported to {service.File.GetFullName()}", "Success",
            MessageButton.OK, MessageIcon.Information);
        }
        catch (Exception e)
        {
          MessageBoxService.Show(e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
      }
    }


    public BaTSGridLayout SaveViewLayout(string name, bool openFirst, bool isDefaultView)
    {
      var layout = new BaTSGridLayout
      {
        ControlName = ParameterViewModel.Title,
        Form = ParameterViewModel.Title,
        Layoutname = name,
        OpenFirst = openFirst,
        UserId = _currentUser.Id,
        Layoutstream = LayoutService.SaveLayout().ToArray(),
        IsDefault = isDefaultView,
      };

      var saved = _customizerRepository.SaveLayout(layout);


      return saved;
    }

    public BaTSGridLayout UpdateViewLayout(BaTSGridLayout layout)
    {
      layout.Layoutstream = LayoutService.SaveLayout().ToArray();
      layout.LastUpdateBy = _currentUser.WindowsLogin;
      layout.LastUpdateOn = DateTime.Now;

      var updated = _customizerRepository.UpdateLayout(layout);

      return updated;
    }

    public void DeleteViewLayout(BaTSGridLayout layout)
    {
      _customizerRepository.DeleteLayout(layout);
    }

    protected void CheckEntityColumns(BaTSControlCustomizer[] columnCustomizers, Type type, bool shouldColumnsBeReadOnly = false)
    {
      var entityColumns = type.GetProperties();

      foreach (var entityColumn in entityColumns)
      {
        if (entityColumn.GetMethod is not null && entityColumn.GetMethod.IsVirtual)
        {
          continue;
        }

        var hasColumn = columnCustomizers.Any(ele =>
          ele.ControlName != null &&
          ele.ControlName.Equals($"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}")
          && ele.ColumnName != null && ele.ColumnName.Equals(entityColumn.Name));

        if (!hasColumn)
        {
          var newControlCustomizerColumn = new BaTSControlCustomizer
          {
            ControlName = $"{ParameterViewModel.GroupTitle}-{ParameterViewModel.Title}",
            ColumnName = entityColumn.Name,
            ColumnTextOrigin = entityColumn.Name,
            ColumnType = Helper.GetUnderlyingType(entityColumn.PropertyType),
            IsReadOnly = shouldColumnsBeReadOnly,
            //ColumnPosition = gridColumn.VisibleIndex,
            ShortHelpText = entityColumn.Name,
            //ColumnWidth = (decimal)gridColumn.ActualWidth
          };
          _customizerRepository.SaveControlCustomizer(newControlCustomizerColumn);
        }
      }
    }
  }
}