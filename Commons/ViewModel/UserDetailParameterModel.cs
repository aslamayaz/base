﻿using DbSets.Security;
using DevExpress.Mvvm;
using System.Collections.ObjectModel;

namespace Commons.ViewModel
{
  public class UserDetailParameter : BindableBase
  {
    public BaTDUser User { get; set; }

    public string GroupTitle { get; set; }

    public ObservableCollection<UserPolicy> UserPolicies { get; set; }

    public UserPolicy SelectedModuleItemPolicy
    {
      get { return GetValue<UserPolicy>(); }
      set { SetValue(value); }
    }

    public ObservableCollection<string> ModuleAccess
    {
      get
      {
        return new ObservableCollection<string>()
        {
          Commons.Enums.ModuleAccess.NoAccess.ToString(),
          Commons.Enums.ModuleAccess.Read.ToString(),
          Commons.Enums.ModuleAccess.ReadWrite.ToString()
        };
      }
    }
  }
}