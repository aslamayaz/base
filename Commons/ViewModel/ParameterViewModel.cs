﻿namespace Commons.ViewModel
{
  public class ParameterViewModel
  {
    public string Title { get; set; }
    public string GroupTitle { get; set; }
  }
}