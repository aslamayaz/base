﻿using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;

namespace Commons.Services
{
  public class ExportService : ServiceBase, IExportService
  {
    #region Implementation of IExportService

    public void Export(string name)
    {
      var view = ((GridControl)AssociatedObject).View;
      ((TableView)view).ExportToXlsx(name);
    }


    #endregion
  }
}