﻿namespace Commons.Services
{
  public interface IExportService
  {
    void Export(string name);
  }
}