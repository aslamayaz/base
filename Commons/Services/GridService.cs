﻿using System.Linq;
using DevExpress.Mvvm.UI;
using DevExpress.Utils;
using DevExpress.Xpf.Grid;

namespace Commons.Services
{
  public class GridService : ServiceBase, IGridService
  {
    #region Implementation of IGridService

    public void AddNewRow()
    {
      var view = ((GridControl)AssociatedObject).View;

      ((GridControl)AssociatedObject).CurrentColumn =
        ((GridControl)AssociatedObject).Columns.First(x => x.AllowEditing == DefaultBoolean.True);

      ((TableView)view).AddNewRow();
      ((TableView)view).ShowEditor();
    }

    public void EditRow()
    {
      var view = ((GridControl)AssociatedObject).View;

      ((GridControl)AssociatedObject).CurrentColumn =
        ((GridControl)AssociatedObject).Columns.First(x => x.AllowEditing == DefaultBoolean.True);

      ((TableView)view).ShowEditor(true);
    }

    #endregion
  }
}