﻿namespace Commons.Services
{
  public interface IGridService
  {
    void AddNewRow();
    void EditRow();
  }
}