﻿using System;
using DevExpress.Mvvm;
using DevExpress.Xpf.Core;

namespace Commons.SplashScreen
{
  public static class SplashScreenHelper
  {
    public static void ShowSplashScreen(string toolName)
    {
      var viewModel = new DXSplashScreenViewModel()
      {
        Logo = new Uri(@"pack://application:,,,/DevExpress.Xpf.DemoBase.v20.1;component/DemoLauncher/Images/Logo.svg"),
        Status = "Starting...",
        Title = toolName,
        Subtitle = "Powered by CTS IT Consulting GmbH & Co. KG",
        Copyright = $"Copyright © {DateTime.Now.Year}, All Rights Reserved.",
        IsIndeterminate = true,
      };

      var manager = SplashScreenManager.CreateFluent(viewModel);
      manager?.ShowOnStartup();
    }
  }
}