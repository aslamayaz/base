﻿namespace Commons.Enums
{
  public enum ModuleAccess
  {
    NoAccess,
    Read,
    ReadWrite
  }
}