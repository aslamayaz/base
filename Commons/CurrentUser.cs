﻿using System.Collections.Generic;
using DbSets.Security;

namespace Commons
{
  public class CurrentUser : ICurrentUser
  {
    #region Implementation of ICurrentUser

    public int Id { get; set; }
    public string Username { get; set; }
    public string WindowsLogin { get; set; }
    public bool IsAdmin { get; set; }
    public bool IsDeveloper { get; set; }
    public string Culture { get; set; }
    public string CultureUI { get; set; }
    public IEnumerable<UserPolicy> UserPolicies { get; set; }
    public string SessionThread { get; set; }

    #endregion
  }
}