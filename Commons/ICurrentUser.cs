﻿using System;
using System.Collections.Generic;
using DbSets.Security;

namespace Commons
{
  public interface ICurrentUser
  {
    int Id { get; set; }
    string Username { get; set; }
    string WindowsLogin { get; set; }
    bool IsAdmin { get; set; }
    bool IsDeveloper { get; set; }
    string Culture { get; set; }
    string CultureUI { get; set; }

    IEnumerable<UserPolicy> UserPolicies { get; set; }
    string SessionThread { get; set; }
  }
}