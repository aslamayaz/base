﻿using Commons.Enums;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Commons.Utils
{
  public class EnumToVisibilityConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return value is ModuleAccess and ModuleAccess.ReadWrite;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}