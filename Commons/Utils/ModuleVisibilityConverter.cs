﻿using System;
using System.Globalization;
using System.Windows.Data;
using Commons.Enums;
using Commons.Interfaces;
using DevExpress.Xpf.Core;

namespace Commons.Utils
{
  public class ModuleVisibilityConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is DXTabItem { DataContext: IStandardView view })
      {
        if (view.CanAdd && view.ModuleAccess == ModuleAccess.ReadWrite)
        {
          return true;
        }
        else
        {
          return false;
        }
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}