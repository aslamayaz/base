﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Commons.Enums;

namespace Commons.Utils
{
  public class AccessRightVisibilityConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is ModuleAccess and ModuleAccess.NoAccess)
      {
        return Visibility.Collapsed;
      }

      return Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}