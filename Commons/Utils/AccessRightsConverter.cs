﻿using Commons.Enums;
using Commons.Interfaces;
using DevExpress.Xpf.Core;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Commons.Utils
{
  public class AccessRightsConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is DXTabItem { DataContext: IStandardView standardView })
      {
        return standardView.ModuleAccess == ModuleAccess.ReadWrite;
      }

      if (value is ModuleAccess access)
      {
        switch (access)
        {
          case ModuleAccess.Read:
            return false;
          case ModuleAccess.ReadWrite:
            return true;
        }
      }

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}