﻿using System;

namespace Commons.Utils
{
  public class Helper
  {
    public static string GetUnderlyingType(Type propertyType)
    {
      if (propertyType == typeof(bool))
      {
        return "bool";
      }
      else if (propertyType == typeof(bool?))
      {
        return "bool";
      }

      return "string";
    }
  }
}