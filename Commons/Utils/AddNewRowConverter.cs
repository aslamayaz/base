﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Commons.Utils
{
  public class AddNewRowConverter : IValueConverter
  {
    #region Implementation of IValueConverter

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is bool and true)
      {
        return "Bottom";
      }

      return "Never";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}