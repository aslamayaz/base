﻿namespace Commons.ColumnCustomizer
{
  public enum ColumnType
  {
    Default,
    Lookup,
    Bool
  }
}