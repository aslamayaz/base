﻿using System.Collections;
using System.Windows.Media;
using DevExpress.Mvvm;

namespace Commons.ColumnCustomizer
{
  public class Column : BindableBase
  {
    public Column(ColumnType columnType, string fieldname, bool isReadOnly)
    {
      ColumnType = columnType;
      Name = fieldname;
      IsReadOnly = isReadOnly;
    }


    public ColumnType ColumnType { get; }

    public string Name { get; }

    public string Header { get; set; }

    public int Width { get; set; }

    public int Position { get; set; }
    public int GroupIndex { get; set; } = -1;

    public bool WordWrap { get; set; }
    public bool IsReadOnly { get; set; }

    public SolidColorBrush BackgroundColor { get; set; }

    public SolidColorBrush HeaderBackground { get; set; }
  }

  public class LookupColumn : Column
  {
    public LookupColumn(ColumnType columnType, string fieldname, IList source, string displayMember, bool isReadOnly)
      : base(columnType, fieldname, isReadOnly)
    {
      Source = source;
      DisplayMember = displayMember;
    }

    public IList Source { get; }
    public string DisplayMember { get; }
  }
}