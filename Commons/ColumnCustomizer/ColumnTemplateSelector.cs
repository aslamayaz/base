﻿using System.Windows;
using System.Windows.Controls;

namespace Commons.ColumnCustomizer
{
  public class ColumnTemplateSelector : DataTemplateSelector
  {
    public DataTemplate DefaultColumnTemplate { get; set; }
    public DataTemplate LookupColumnTemplate { get; set; }
    public DataTemplate BoolColumnTemplate { get; set; }

    public override DataTemplate SelectTemplate(object item, DependencyObject container)
    {
      var column = item as Column;

      if (column == null)
      {
        return null;
      }

      switch (column.ColumnType)
      {
        case ColumnType.Default:
          return DefaultColumnTemplate;
        case ColumnType.Lookup:
          return LookupColumnTemplate;
        case ColumnType.Bool:
          return BoolColumnTemplate;
      }
      return null;
    }
  }
}