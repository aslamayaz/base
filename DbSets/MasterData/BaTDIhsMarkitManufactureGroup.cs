﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitManufactureGroup
    {
        public int Id { get; set; }
        public string IhsmanufacturerGroup { get; set; }
        public int? BasfmanufactureGroupId { get; set; }
        public bool? ToCheck { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
