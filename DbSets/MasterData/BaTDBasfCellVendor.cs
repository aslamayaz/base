﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDBasfCellVendor
    {
        public BaTDBasfCellVendor()
        {
            BaTDIhsMarkitCompanies = new HashSet<BaTDIhsMarkitCompany>();
        }

        public int Id { get; set; }
        public string CellVendor { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }

        public virtual ICollection<BaTDIhsMarkitCompany> BaTDIhsMarkitCompanies { get; set; }
    }
}
