﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDCurrency
    {
        public int Id { get; set; }
        public string Currency { get; set; }
        public DateTime? DscreatedDt { get; set; }
        public string ChangedBy { get; set; }
        public DateTime? ChangedDt { get; set; }
        public string CurrencySymbol { get; set; }
    }
}
