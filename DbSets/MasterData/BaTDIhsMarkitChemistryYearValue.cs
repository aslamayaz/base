﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitChemistryYearValue
    {
        public int Id { get; set; }
        public int IhsmarkitChemistryId { get; set; }
        public int? YearValue { get; set; }
        public decimal? MAhvalue { get; set; }
        public decimal? Voltage { get; set; }
        public decimal? KwhKgvalue { get; set; }
        public int? AddedByUserId { get; set; }
        public DateTime? AddedOn { get; set; }
        public int? ChangedByUserId { get; set; }
        public DateTime? ChangedOn { get; set; }

        public virtual BaTDIhsMarkitChemistry IhsmarkitChemistry { get; set; }
    }
}
