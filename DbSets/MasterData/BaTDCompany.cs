﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDCompany
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public bool? InHousePcam { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
