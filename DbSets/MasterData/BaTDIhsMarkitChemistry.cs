﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitChemistry
    {
        public BaTDIhsMarkitChemistry()
        {
            BaTDIhsMarkitChemistryYearValues = new HashSet<BaTDIhsMarkitChemistryYearValue>();
        }

        public int Id { get; set; }
        public string Ihschemistry { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }

        public virtual ICollection<BaTDIhsMarkitChemistryYearValue> BaTDIhsMarkitChemistryYearValues { get; set; }
    }
}
