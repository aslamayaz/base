﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitCompany
    {
        public int Id { get; set; }
        public string IhscellVendor { get; set; }
        public int? BasfcellVendorId { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public bool? ToCheck { get; set; }

        public virtual BaTDBasfCellVendor BasfcellVendor { get; set; }
    }
}
