﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitPowerTrain
    {
        public int Id { get; set; }
        public string PropulsionSystemDesign { get; set; }
        public string SystemSubDesign { get; set; }
        public string GlobalUid { get; set; }
        public string BasfpowerTrain { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public int? OrderPosition { get; set; }
    }
}
