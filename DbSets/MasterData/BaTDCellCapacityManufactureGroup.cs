﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDCellCapacityManufactureGroup
    {
        public int Id { get; set; }
        public string CellCapacityManufacturerGroup { get; set; }
        public int? BasfmanufactureGroupId { get; set; }
        public bool? ToCheck { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
