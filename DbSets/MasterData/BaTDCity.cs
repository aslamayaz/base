﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDCity
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public int? CountryId { get; set; }
    }
}
