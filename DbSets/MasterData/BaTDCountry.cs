﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDCountry
    {
        public int Id { get; set; }
        public string CountryShort { get; set; }
        public string CountryLong { get; set; }
        public int? RegionId { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public bool? ToCheck { get; set; }
    }
}
