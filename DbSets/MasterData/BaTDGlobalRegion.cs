﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDGlobalRegion
    {
        public int Id { get; set; }
        public string RegionShort { get; set; }
        public string RegionLong { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
