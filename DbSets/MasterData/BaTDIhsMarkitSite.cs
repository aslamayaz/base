﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.MasterData
{
    public partial class BaTDIhsMarkitSite
    {
        public int Id { get; set; }
        public string Ihssite { get; set; }
        public string Ihsregion { get; set; }
        public string GlobalUid { get; set; }
        public int? BasfregionId { get; set; }
        public bool? ToCheck { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
