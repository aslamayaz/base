﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DbSets.MasterData
{
    public partial class MasterDataContext : DbContext
    {
        public MasterDataContext()
        {
        }

        public MasterDataContext(DbContextOptions<MasterDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BaTDBasfCellVendor> BaTDBasfCellVendors { get; set; }
        public virtual DbSet<BaTDBasfManufacturerGroup> BaTDBasfManufacturerGroups { get; set; }
        public virtual DbSet<BaTDCellCapacityManufactureGroup> BaTDCellCapacityManufactureGroups { get; set; }
        public virtual DbSet<BaTDCity> BaTDCities { get; set; }
        public virtual DbSet<BaTDCompany> BaTDCompanies { get; set; }
        public virtual DbSet<BaTDCountry> BaTDCountries { get; set; }
        public virtual DbSet<BaTDCurrency> BaTDCurrencies { get; set; }
        public virtual DbSet<BaTDGlobalRegion> BaTDGlobalRegions { get; set; }
        public virtual DbSet<BaTDIhsMarkitChemistry> BaTDIhsMarkitChemistries { get; set; }
        public virtual DbSet<BaTDIhsMarkitChemistryYearValue> BaTDIhsMarkitChemistryYearValues { get; set; }
        public virtual DbSet<BaTDIhsMarkitCompany> BaTDIhsMarkitCompanies { get; set; }
        public virtual DbSet<BaTDIhsMarkitManufactureGroup> BaTDIhsMarkitManufactureGroups { get; set; }
        public virtual DbSet<BaTDIhsMarkitPowerTrain> BaTDIhsMarkitPowerTrains { get; set; }
        public virtual DbSet<BaTDIhsMarkitRegion> BaTDIhsMarkitRegions { get; set; }
        public virtual DbSet<BaTDIhsMarkitSite> BaTDIhsMarkitSites { get; set; }
        public virtual DbSet<BaTDRegion> BaTDRegions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=WIN10059204;Database=BatMaps_Test;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<BaTDBasfCellVendor>(entity =>
            {
                entity.ToTable("BA_T_D_BASF_CellVendors", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CellVendor).HasMaxLength(150);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDBasfManufacturerGroup>(entity =>
            {
                entity.ToTable("BA_T_D_BASF_ManufacturerGroup", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.ManufacturerGroup).HasMaxLength(150);
            });

            modelBuilder.Entity<BaTDCellCapacityManufactureGroup>(entity =>
            {
                entity.ToTable("BA_T_D_Cell_Capacity_ManufactureGroup", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasfmanufactureGroupId).HasColumnName("BASFManufactureGroupID");

                entity.Property(e => e.CellCapacityManufacturerGroup).HasMaxLength(150);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDCity>(entity =>
            {
                entity.ToTable("BA_T_D_City", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CityName).HasMaxLength(150);

                entity.Property(e => e.CountryId).HasColumnName("CountryID");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDCompany>(entity =>
            {
                entity.ToTable("BA_T_D_Company", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CompanyName).HasMaxLength(100);

                entity.Property(e => e.InHousePcam).HasColumnName("InHousePCAM");

                entity.Property(e => e.IsActive).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDCountry>(entity =>
            {
                entity.ToTable("BA_T_D_Country", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CountryLong).HasMaxLength(255);

                entity.Property(e => e.CountryShort).HasMaxLength(20);

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.ToCheck).HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<BaTDCurrency>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BA_T_D_Currency", "MasterData");

                entity.Property(e => e.ChangedBy).HasMaxLength(50);

                entity.Property(e => e.ChangedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("ChangedDT");

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CurrencySymbol).HasMaxLength(5);

                entity.Property(e => e.DscreatedDt)
                    .HasColumnType("datetime")
                    .HasColumnName("DSCreatedDT");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");
            });

            modelBuilder.Entity<BaTDGlobalRegion>(entity =>
            {
                entity.ToTable("BA_T_D_Global_Region", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.RegionLong).HasMaxLength(255);

                entity.Property(e => e.RegionShort).HasMaxLength(20);
            });

            modelBuilder.Entity<BaTDIhsMarkitChemistry>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_Chemistry", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Category).HasMaxLength(50);

                entity.Property(e => e.Ihschemistry)
                    .HasMaxLength(50)
                    .HasColumnName("IHSChemistry");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.SubCategory).HasMaxLength(50);
            });

            modelBuilder.Entity<BaTDIhsMarkitChemistryYearValue>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_Chemistry_YearValues", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AddedByUserId).HasColumnName("AddedByUserID");

                entity.Property(e => e.AddedOn).HasColumnType("datetime");

                entity.Property(e => e.ChangedByUserId).HasColumnName("ChangedByUserID");

                entity.Property(e => e.ChangedOn).HasColumnType("datetime");

                entity.Property(e => e.IhsmarkitChemistryId).HasColumnName("IHSMarkitChemistryID");

                entity.Property(e => e.KwhKgvalue)
                    .HasColumnType("decimal(18, 3)")
                    .HasColumnName("kwhKGValue");

                entity.Property(e => e.MAhvalue)
                    .HasColumnType("decimal(18, 3)")
                    .HasColumnName("mAHValue");

                entity.Property(e => e.Voltage).HasColumnType("decimal(18, 3)");

                entity.HasOne(d => d.IhsmarkitChemistry)
                    .WithMany(p => p.BaTDIhsMarkitChemistryYearValues)
                    .HasForeignKey(d => d.IhsmarkitChemistryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BA_T_D_IHS_Markit_Chemistry_YearValues");
            });

            modelBuilder.Entity<BaTDIhsMarkitCompany>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_Companies", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasfcellVendorId).HasColumnName("BASFCellVendorID");

                entity.Property(e => e.IhscellVendor)
                    .HasMaxLength(100)
                    .HasColumnName("IHSCellVendor");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.ToCheck).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.BasfcellVendor)
                    .WithMany(p => p.BaTDIhsMarkitCompanies)
                    .HasForeignKey(d => d.BasfcellVendorId)
                    .HasConstraintName("FK_BA_T_D_IHS_Markit_Companies_BA_T_D_BASF_CellVendors");
            });

            modelBuilder.Entity<BaTDIhsMarkitManufactureGroup>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_ManufactureGroup", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasfmanufactureGroupId).HasColumnName("BASFManufactureGroupID");

                entity.Property(e => e.IhsmanufacturerGroup)
                    .HasMaxLength(150)
                    .HasColumnName("IHSManufacturerGroup");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDIhsMarkitPowerTrain>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_PowerTrain", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasfpowerTrain)
                    .HasMaxLength(50)
                    .HasColumnName("BASFPowerTrain");

                entity.Property(e => e.GlobalUid)
                    .HasMaxLength(200)
                    .HasColumnName("GlobalUID");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.PropulsionSystemDesign).HasMaxLength(100);

                entity.Property(e => e.SystemSubDesign).HasMaxLength(100);
            });

            modelBuilder.Entity<BaTDIhsMarkitRegion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BA_T_D_IHS_Markit_Regions", "MasterData");

                entity.Property(e => e.BasfregionId).HasColumnName("BASFRegionID");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.Ihsregion)
                    .HasMaxLength(100)
                    .HasColumnName("IHSRegion");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDIhsMarkitSite>(entity =>
            {
                entity.ToTable("BA_T_D_IHS_Markit_Sites", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BasfregionId).HasColumnName("BASFRegionID");

                entity.Property(e => e.GlobalUid)
                    .HasMaxLength(200)
                    .HasColumnName("GlobalUID");

                entity.Property(e => e.Ihsregion)
                    .HasMaxLength(100)
                    .HasColumnName("IHSRegion");

                entity.Property(e => e.Ihssite)
                    .HasMaxLength(100)
                    .HasColumnName("IHSSite");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<BaTDRegion>(entity =>
            {
                entity.ToTable("BA_T_D_Region", "MasterData");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

                entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

                entity.Property(e => e.RegionLong).HasMaxLength(255);

                entity.Property(e => e.RegionShort).HasMaxLength(20);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
