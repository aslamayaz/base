﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DbSets.Customizer
{
  public partial class CustomizerContext : DbContext
  {
    public CustomizerContext()
    {
    }

    public CustomizerContext(DbContextOptions<CustomizerContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BaTSControlCustomizer> BaTSControlCustomizers { get; set; }
    public virtual DbSet<BaTSGridLayout> BaTSGridLayouts { get; set; }
    public virtual DbSet<BaTSGridLayoutShareRegion> BaTSGridLayoutShareRegions { get; set; }
    public virtual DbSet<BaTSGridLayoutShareUser> BaTSGridLayoutShareUsers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        optionsBuilder.UseSqlServer(ConnectionString.DbConnection);
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

      modelBuilder.Entity<BaTSControlCustomizer>(entity =>
      {
        entity.ToTable("BA_T_S_ControlCustomizer", "Customizer");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.BandName).HasMaxLength(255);

        entity.Property(e => e.CellBackground).HasDefaultValueSql("((0))");

        entity.Property(e => e.ColumnName).HasMaxLength(255);

        entity.Property(e => e.ColumnTextCustomize).HasMaxLength(255);

        entity.Property(e => e.ColumnTextOrigin).HasMaxLength(255);

        entity.Property(e => e.ColumnType).HasMaxLength(100);

        entity.Property(e => e.ColumnWidth).HasColumnType("decimal(18, 0)");

        entity.Property(e => e.ControlName).HasMaxLength(255);

        entity.Property(e => e.Datasource).HasMaxLength(255);

        entity.Property(e => e.DbcolumnName)
                  .HasMaxLength(255)
                  .HasColumnName("DBColumnName");

        entity.Property(e => e.DbtableName)
                  .HasMaxLength(255)
                  .HasColumnName("DBTableName");

        entity.Property(e => e.ExcelWidth).HasColumnType("decimal(18, 2)");

        entity.Property(e => e.HeaderBackGround).HasDefaultValueSql("((0))");

        entity.Property(e => e.IsReadOnly).HasDefaultValueSql("((0))");

        entity.Property(e => e.LastFound).HasColumnType("datetime");

        entity.Property(e => e.LongHelpText).HasColumnType("ntext");

        entity.Property(e => e.ShortHelpText).HasMaxLength(255);

        entity.Property(e => e.Status).HasMaxLength(255);
      });

      modelBuilder.Entity<BaTSGridLayout>(entity =>
      {
        entity.HasNoKey();

        entity.ToTable("BA_T_S_Grid_Layout", "Customizer");

        entity.Property(e => e.ControlName).HasMaxLength(255);

        entity.Property(e => e.Form).HasMaxLength(255);

        entity.Property(e => e.Id)
                  .ValueGeneratedOnAdd()
                  .HasColumnName("ID");

        entity.Property(e => e.IsDefault).HasDefaultValueSql("((0))");

        entity.Property(e => e.IsSelected).HasDefaultValueSql("((0))");

        entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

        entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

        entity.Property(e => e.Layoutname).HasMaxLength(100);

        entity.Property(e => e.OpenFirst).HasDefaultValueSql("((0))");

        entity.Property(e => e.Stamp)
                  .IsRowVersion()
                  .IsConcurrencyToken();

        entity.Property(e => e.UserId).HasColumnName("UserID");
      });

      modelBuilder.Entity<BaTSGridLayoutShareRegion>(entity =>
      {
        entity.HasNoKey();

        entity.ToTable("BA_T_S_Grid_Layout_Share_Region", "Customizer");

        entity.Property(e => e.GlobalRegionId).HasColumnName("GlobalRegionID");

        entity.Property(e => e.Id)
                  .ValueGeneratedOnAdd()
                  .HasColumnName("ID");

        entity.Property(e => e.LayoutId).HasColumnName("LayoutID");
      });

      modelBuilder.Entity<BaTSGridLayoutShareUser>(entity =>
      {
        entity.HasNoKey();

        entity.ToTable("BA_T_S_Grid_Layout_Share_User", "Customizer");

        entity.Property(e => e.Id)
                  .ValueGeneratedOnAdd()
                  .HasColumnName("ID");

        entity.Property(e => e.LayoutId).HasColumnName("LayoutID");

        entity.Property(e => e.UserId).HasColumnName("UserID");
      });

      OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
  }
}
