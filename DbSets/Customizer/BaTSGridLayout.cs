﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Customizer
{
    public partial class BaTSGridLayout
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Form { get; set; }
        public string ControlName { get; set; }
        public string Layoutname { get; set; }
        public byte[] Layoutstream { get; set; }
        public bool? IsDefault { get; set; }
        public bool? IsSelected { get; set; }
        public bool? OpenFirst { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public byte[] Stamp { get; set; }
    }
}
