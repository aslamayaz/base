﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Customizer
{
    public partial class BaTSGridLayoutShareRegion
    {
        public int Id { get; set; }
        public int LayoutId { get; set; }
        public int GlobalRegionId { get; set; }
        public bool? OpenFirst { get; set; }
    }
}
