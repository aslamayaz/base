﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTDUserGroup
    {
        public BaTDUserGroup()
        {
            BaTDUsers = new HashSet<BaTDUser>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }

        public virtual ICollection<BaTDUser> BaTDUsers { get; set; }
    }
}
