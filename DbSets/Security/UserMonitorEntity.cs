﻿namespace DbSets.Security
{
  public class UserMonitorEntity
  {
    public string GlobalRegion { get; set; }
    public string WindowsLogin { get; set; }
    public string Firstname { get; set; }
    public string LastName { get; set; }
    public string LastContact { get; set; }
    public string UsedClickOnceVersion { get; set; }
    public string ClickOnceVersion { get; set; }
    public string UsedDLL { get; set; }
    public string CloseApplication { get; set; }
    public string SendMessage { get; set; }
    public string MessageRead { get; set; }
    public string MessageSent { get; set; }
    public string UserID { get; set; }
    public string SessionThread { get; set; }
  }
}