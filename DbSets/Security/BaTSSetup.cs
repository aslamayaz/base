﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTSSetup
    {
        public int Id { get; set; }
        public string Area { get; set; }
        public string Criteria { get; set; }
        public string Value { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
        public string Comment { get; set; }
    }
}
