﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTSUserModule
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ModuleId { get; set; }
        public string AccessRight { get; set; }
        public DateTime? LastUpdateOn { get; set; }
        public string LastUpdateBy { get; set; }
    }
}
