﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTSModuleGroup
    {
        public BaTSModuleGroup()
        {
            BaTSModules = new HashSet<BaTSModule>();
        }

        public int Id { get; set; }
        public string Groupname { get; set; }
        public string Description { get; set; }
        public string AlternativeCaption { get; set; }
        public int? OrderPosition { get; set; }

        public virtual ICollection<BaTSModule> BaTSModules { get; set; }
    }
}
