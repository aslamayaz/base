﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTSModule
    {
        public int Id { get; set; }
        public string MenuItemGroup { get; set; }
        public string MenuItemName { get; set; }
        public string NavBarItemName { get; set; }
        public string FormName { get; set; }
        public int? GroupId { get; set; }
        public string AccessDescription { get; set; }
        public bool? ActiveCheck { get; set; }
        public string AlternativeCaption { get; set; }
        public string ShortHelpText { get; set; }
        public string LongHelpText { get; set; }
        public int? OrderPosition { get; set; }
        public string IconImageName { get; set; }

        public virtual BaTSModuleGroup Group { get; set; }
    }
}
