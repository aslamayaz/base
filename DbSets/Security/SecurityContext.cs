﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace DbSets.Security
{
  public partial class SecurityContext : DbContext
  {
    public SecurityContext()
    {
    }

    public SecurityContext(DbContextOptions<SecurityContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BaTDConnectedUser> BaTDConnectedUsers { get; set; }
    public virtual DbSet<BaTDUser> BaTDUsers { get; set; }
    public virtual DbSet<BaTDUserGroup> BaTDUserGroups { get; set; }
    public virtual DbSet<BaTSModule> BaTSModules { get; set; }
    public virtual DbSet<BaTSModuleGroup> BaTSModuleGroups { get; set; }
    public virtual DbSet<BaTSSetup> BaTSSetups { get; set; }
    public virtual DbSet<BaTSUserModule> BaTSUserModules { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        optionsBuilder.UseSqlServer(ConnectionString.DbConnection);
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

      modelBuilder.Entity<BaTDConnectedUser>(entity =>
      {
        entity.HasNoKey();

        entity.ToTable("BA_t_D_ConnectedUsers", "Security");

        entity.Property(e => e.LastContact).HasColumnType("datetime");

        entity.Property(e => e.MessageRead).HasColumnType("datetime");

        entity.Property(e => e.SendMessage).HasMaxLength(500);

        entity.Property(e => e.SessionThread)
                  .IsRequired()
                  .HasMaxLength(255);

        entity.Property(e => e.Start).HasColumnType("datetime");

        entity.Property(e => e.UserId).HasColumnName("UserID");
      });

      modelBuilder.Entity<BaTDUser>(entity =>
      {
        entity.ToTable("BA_T_D_Users", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.AllowToChangeCurrency).HasDefaultValueSql("((1))");

        entity.Property(e => e.ClickOnceVersion).HasMaxLength(100);

        entity.Property(e => e.Culture)
                  .HasMaxLength(10)
                  .HasDefaultValueSql("('en-us')");

        entity.Property(e => e.CultureUi)
                  .HasMaxLength(10)
                  .HasColumnName("CultureUI")
                  .HasDefaultValueSql("('en')");

        entity.Property(e => e.Email).HasMaxLength(255);

        entity.Property(e => e.Firstname).HasMaxLength(100);

        entity.Property(e => e.GlobalRegionId).HasColumnName("GlobalRegionID");

        entity.Property(e => e.GroupId).HasColumnName("GroupID");

        entity.Property(e => e.LastLogin).HasColumnType("datetime");

        entity.Property(e => e.LastName).HasMaxLength(100);

        entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

        entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

        entity.Property(e => e.StandardCurrencyId).HasColumnName("StandardCurrencyID");

        entity.Property(e => e.UsedDll).HasColumnName("UsedDLL");

        entity.Property(e => e.WindowsLogin).HasMaxLength(200);

        entity.HasOne(d => d.Group)
                  .WithMany(p => p.BaTDUsers)
                  .HasForeignKey(d => d.GroupId)
                  .HasConstraintName("FK__BA_T_D_Us__Group__5224328E");
      });

      modelBuilder.Entity<BaTDUserGroup>(entity =>
      {
        entity.ToTable("BA_T_D_User_Groups", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.GroupName).HasMaxLength(100);

        entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

        entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");
      });

      modelBuilder.Entity<BaTSModule>(entity =>
      {
        entity.ToTable("BA_T_S_Module", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.AccessDescription).HasColumnType("ntext");

        entity.Property(e => e.AlternativeCaption).HasMaxLength(255);

        entity.Property(e => e.FormName).HasMaxLength(50);

        entity.Property(e => e.GroupId).HasColumnName("GroupID");

        entity.Property(e => e.IconImageName).HasMaxLength(100);

        entity.Property(e => e.MenuItemGroup).HasMaxLength(255);

        entity.Property(e => e.MenuItemName).HasMaxLength(255);

        entity.Property(e => e.NavBarItemName).HasMaxLength(255);

        entity.Property(e => e.ShortHelpText).HasMaxLength(255);

        entity.HasOne(d => d.Group)
                  .WithMany(p => p.BaTSModules)
                  .HasForeignKey(d => d.GroupId)
                  .HasConstraintName("FK_BA_T_S_Module_BA_T_S_Module_Group");
      });

      modelBuilder.Entity<BaTSModuleGroup>(entity =>
      {
        entity.ToTable("BA_T_S_Module_Group", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.AlternativeCaption).HasMaxLength(255);

        entity.Property(e => e.Description)
                  .HasMaxLength(10)
                  .IsFixedLength(true);

        entity.Property(e => e.Groupname).HasMaxLength(255);
      });

      modelBuilder.Entity<BaTSSetup>(entity =>
      {
        entity.ToTable("BA_T_S_Setup", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.Area).HasMaxLength(100);

        entity.Property(e => e.Criteria).HasMaxLength(100);

        entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

        entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

        entity.Property(e => e.Value).HasMaxLength(2000);
      });

      modelBuilder.Entity<BaTSUserModule>(entity =>
      {
        entity.ToTable("BA_T_S_User_Modules", "Security");

        entity.Property(e => e.Id).HasColumnName("ID");

        entity.Property(e => e.AccessRight).HasMaxLength(20);

        entity.Property(e => e.LastUpdateBy).HasMaxLength(100);

        entity.Property(e => e.LastUpdateOn).HasColumnType("datetime");

        entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

        entity.Property(e => e.UserId).HasColumnName("UserID");
      });

      modelBuilder.Entity<UserPolicy>().HasNoKey().ToView(null);
      modelBuilder.Entity<UserMonitorEntity>().HasNoKey().ToView(null);

      OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
  }
}
