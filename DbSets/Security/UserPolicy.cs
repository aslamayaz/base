﻿namespace DbSets.Security
{
  public class UserPolicy
  {
    public string UserGlobalRegion { get; set; }
    public string UserName { get; set; }
    public int UserID { get; set; }
    public string AccessRight { get; set; }
    public string FormName { get; set; }
    public int MenuItemGroupID { get; set; }
    public string MenuItemGroup { get; set; }
    public int MenuGroupOrderPosition { get; set; }
    public string MenuItemName { get; set; }
    public string NavBarItemName { get; set; }
    public int ModuleID { get; set; }
    public int MenuItemOrderPosition { get; set; }
    public string ShortHelpText { get; set; }

    public string LongHelpText { get; set; }
    public string IconImageName { get; set; }

  }
}