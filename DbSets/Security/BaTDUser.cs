﻿using System;

#nullable disable

namespace DbSets.Security
{
  public partial class BaTDUser
  {
    public int Id { get; set; }
    public string LastName { get; set; }
    public string Firstname { get; set; }
    public string WindowsLogin { get; set; }
    public int? GroupId { get; set; }
    public DateTime? LastLogin { get; set; }
    public int? GlobalRegionId { get; set; }
    public int? StandardCurrencyId { get; set; }
    public bool? AllowToChangeCurrency { get; set; }
    public string Culture { get; set; }
    public string CultureUi { get; set; }
    public string Email { get; set; }
    public bool? UsedClickOnceVersion { get; set; }
    public string ClickOnceVersion { get; set; }
    public string UsedDll { get; set; }
    public bool? IsActive { get; set; }
    public DateTime? LastUpdateOn { get; set; }
    public string LastUpdateBy { get; set; }

    public virtual BaTDUserGroup Group { get; set; }
  }
}
