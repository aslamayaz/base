﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DbSets.Security
{
    public partial class BaTDConnectedUser
    {
        public int UserId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? LastContact { get; set; }
        public bool? CloseApplication { get; set; }
        public string SendMessage { get; set; }
        public DateTime? MessageRead { get; set; }
        public bool? MessageSent { get; set; }
        public string SessionThread { get; set; }
    }
}
