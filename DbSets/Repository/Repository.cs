﻿using Microsoft.EntityFrameworkCore;

namespace DbSets.Repository
{
  public abstract class Repository : IRepository
  {
    #region Implementation of IRepository

    public abstract DbContext Context { get; }
    #endregion
  }
}