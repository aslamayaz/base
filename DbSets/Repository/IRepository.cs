﻿using Microsoft.EntityFrameworkCore;

namespace DbSets.Repository
{
  public interface IRepository
  {
    DbContext Context { get; }
  }
}